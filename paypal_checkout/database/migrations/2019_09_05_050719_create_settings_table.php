<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('store_id');
            $table->integer('global');
            $table->integer('force_payment');
            $table->decimal('shipping_cost', 10, 2);
            $table->integer('paypal_mode');
            $table->string('live_client_id', 1000);
            $table->string('live_client_secret', 1000);
            $table->string('sb_client_id', 1000);
            $table->string('sb_client_secret', 1000);
            $table->string('header_script', 2000);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('settings');
    }
}
