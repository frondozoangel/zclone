<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterSettingsDefault extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('settings', function (Blueprint $table) {
            $table->integer('global')->default(0)->change();
            $table->integer('force_payment')->default(0)->change();
            $table->decimal('shipping_cost', 10, 2)->default(0.00)->change();
            $table->integer('paypal_mode')->default(1)->change();
            $table->string('live_client_id', 1000)->default(encrypt(''))->change();
            $table->string('live_client_secret', 1000)->default(encrypt(''))->change();
            $table->string('sb_client_id', 1000)->default(encrypt(''))->change();
            $table->string('sb_client_secret', 1000)->default(encrypt(''))->change();
            $table->string('header_script', 2000)->default('')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
