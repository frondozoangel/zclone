<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use DB;
use App\Store;
use App\Setting;
use App\User;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    /*public function __construct()
    {
        $this->middleware('auth');
    }*/

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        //$user = Auth::user();
        //echo "<pre>".print_r(compact('user'),true)."</pre>"; die();

            $shopifyUser = Auth::user();
            //print_r($shopifyUser->id);
            $userid = $shopifyUser->id;
            //$shopifyUser = Auth::user();
            //$userid = $shopifyUser->id;
            $store_id = DB::table('store_users')
                ->where('user_id', $userid)
                ->first()->store_id;
            $settings = DB::table('settings')
                ->where('store_id', $store_id)
                ->first();
            $pp_mode = '';
            $pp_m_class = '';
            if ($settings->paypal_mode == 1) {
                $pp_mode = 'Live';
                $pp_m_class = ' Polaris-Button--primary';
            } else {
                $pp_mode = 'Sandbox';
            }
            $global_status = array();
            $gbl_m_class = '';
            if ($settings->global == 1) {
                $global_status = array('enabled', 'Disable');
            } else {
                $global_status = array('disabled', 'Enable');
                $gbl_m_class = ' Polaris-Button--primary';
            }
            $fp_status = array();
            $fpc_m_class = '';
            if ($settings->force_payment == 1) {
                $fp_status = array('enabled', 'Disable');
            } else {
                $fp_status = array('disabled', 'Enable');
                $fpc_m_class = ' Polaris-Button--primary';
            }
            return view('settings', ['s_id' => $store_id, 'settings' => $settings, 'pp_mode' => $pp_mode, 'pp_id' => $settings->paypal_mode, 'pp_m_class' => $pp_m_class, 'global_id' => $settings->global, 'global_status' => $global_status, 'gbl_m_class' => $gbl_m_class, 'fp_id' => $settings->force_payment, 'fp_status' => $fp_status, 'fpc_m_class' => $fpc_m_class, 'shipping' => $settings->shipping_cost, 'delay_timer' => $settings->delay_timer, 'webhook_url' => $settings->webhook_uri]);

    }

    public function save(Request $request){
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $queries = $request->all();

            $data = array();
            $c = 0;
            $exclude = array('_token', 'id');
            $to_enc = array('live_client_id', 'live_client_secret', 'sb_client_id', 'sb_client_secret');
            foreach ($queries as $key => $value) {
                if (!in_array($key, $exclude)) {
                    if ($value != '') {
                        if (in_array($key, $to_enc)) {
                            $data[$key] = encrypt($value);
                        } else {
                            $data[$key] = $value;
                        }
                    }
                }
            }

            $store_id = $request->input('id');
            $setting = Setting::where('store_id',$store_id)->update($data);

            if($setting > 0){
                return redirect('home')->with('status', 'Settings updated!');
            } else {
                return Redirect::back()->withErrors(['msg', 'Something went wrong. Please contact the administrator.']);
            }
        }
    }

    public function change_paypal_mode(Request $request){
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $store_id = $request->input('id');
            $mode = $request->input('mode');
            $setting = Setting::where('store_id',$store_id)->update(array("paypal_mode"=>$mode));
            $output = array();
            $t = '';
            if($mode == 1) {
              $t = 'Live';  
            } else { $t = 'Sandbox'; }
            if ($setting > 0) {
                $output = array('status' => 200, 'msg' => 'PayPal Mode has been changed.', 'mode' => array($t,$mode));
            } else {
                $output = array('status' => 500, 'msg' => 'Something went wrong. Please contact the administrator.', 'mode' => array(null,$mode));
            }
            echo json_encode($output);
            exit();
        }
    }

    public function change_global_ppc_status(Request $request){
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $store_id = $request->input('id');
            $mode = $request->input('mode');
            $setting = Setting::where('store_id',$store_id)->update(array("global"=>$mode));
            $output = array();
            $t = array();
            if($mode == 1) {
              $t[0] = 'enabled';
              $t[1] = 'Disable';  
            } else { $t[0] = 'disabled'; $t[1] = 'Enable'; }
            if ($setting > 0) {
                $output = array('status' => 200, 'msg' => 'Global PayPal Checkout has been '.$t[0].'.', 'mode' => array($t,$mode));
            } else {
                $output = array('status' => 500, 'msg' => 'Something went wrong. Please contact the administrator.', 'mode' => array(null,$mode));
            }
            echo json_encode($output);
            exit();
        }
    }

    public function change_fpc_status(Request $request){
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $store_id = $request->input('id');
            $mode = $request->input('mode');
            $setting = Setting::where('store_id',$store_id)->update(array("force_payment"=>$mode));
            $output = array();
            $t = array();
            if($mode == 1) {
              $t[0] = 'enabled';
              $t[1] = 'Disable';
            } else { $t[0] = 'disabled'; $t[1] = 'Enable'; }
            if ($setting > 0) {
                $output = array('status' => 200, 'msg' => 'Force Payment Capture has been '.$t[0].'.', 'mode' => array($t,$mode));
            } else {
                $output = array('status' => 500, 'msg' => 'Something went wrong. Please contact the administrator.', 'mode' => array(null,$mode));
            }
            echo json_encode($output);
            exit();
        }
    }

    public function doLogin($request) {
        /*$shop = '';
        if ($request && $request['shop']) {
            $shop = $request['shop'];
        }
        $store = DB::table('stores')->where('domain', $shop)->first();
        $store_user = DB::table('store_users')->where('store_id', $store->id)->first();
        $user = DB::table('users')->where('users', $store_user->user_id)->first();
        $userpass = array(
            'email' => $user->email,
            'password' => $user->password
        );
        Auth::login($user, true);*/
    }

    public function decrypt(){
        //echo "<pre>".print_r($_REQUEST,true)."</pre>";
        $data = $_REQUEST;
        if(isset($data['str'])){
          $str = $data['str'];
          $decrypted = decrypt($str);
          echo json_encode(compact('str','decrypted')); die();
        }
    }

    public function encrypt(){
        //echo "<pre>".print_r($_REQUEST,true)."</pre>";
        $data = $_REQUEST;
        if(isset($data['str'])){
          $str = $data['str'];
          $encrypted = encrypt($str);
          echo json_encode(compact('str','encrypted')); die();
        }
    }
}