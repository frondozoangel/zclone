<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\UserProviders;
use App\Store;
use App\Setting;
use App\Http\Controllers\Controller;
use Socialite;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DB;
use GuzzleHttp\Client;

class LoginShopifyController extends Controller
{

    /**
     * Redirect the user to the GitHub authentication page.
     *
     * @return \Illuminate\Http\Response
     */
    public function redirectToProvider(Request $request)
    {

        $this->validate($request, [
            'shop' => 'string|required'
        ]);

        $config = new \SocialiteProviders\Manager\Config(
            env('SHOPIFY_KEY'),
            env('SHOPIFY_SECRET'),
            env('SHOPIFY_REDIRECT'),
            ['subdomain' => str_replace(".myshopify.com","",$request->get('shop'))]
        );

        return Socialite::with('shopify')
            ->setConfig($config)
            ->scopes(['read_content', 'write_content', 'read_themes', 'write_themes', 'read_products', 'write_products', 'read_customers', 'write_customers', 'read_draft_orders', 'write_draft_orders', 'read_orders', 'write_orders', 'read_script_tags', 'write_script_tags', 'read_fulfillments', 'write_fulfillments', 'read_shipping', 'write_shipping', 'read_checkouts', 'write_checkouts'])
            ->redirect();
    }

    /**
     * Obtain the user information from GitHub.
     *
     * @return \Illuminate\Http\Response
     */
    public function handleProviderCallback()
    {

        $shopifyUser = Socialite::driver('shopify')->user();

        // Create user
        $user = User::firstOrCreate([
            'name' => $shopifyUser->nickname,
            'email' => $shopifyUser->email,
            'password' => '',
        ]);

        // Create shopify webhooks.
        $this->createShopifyWebhooks([
          'shop' => $shopifyUser->nickname,
          'oauth_token' => $shopifyUser->token
        ]);

        $exists = false;
        if(UserProviders::where('provider_token', '=', $shopifyUser->token)->exists()){
            $exists = true;
        }

        if ($exists == true) {
            Auth::login($user, true);
            return redirect('/home');
        } else {

            // Store the OAuth Identity
            UserProviders::firstOrCreate([
                'user_id' => $user->id,
                'provider' => 'shopify',
                'provider_user_id' => $shopifyUser->id,
                'provider_token' => $shopifyUser->token,
            ]);

            // create store record.
            $shop = Store::firstOrCreate([
                'name' => $shopifyUser->name,
                'domain' => $shopifyUser->nickname,
            ]);

            // create settings record.
            Setting::firstOrCreate([
                'store_id' => $shop->id,
            ]);

            // create tokens record.
            $token = DB::table('tokens')->where('shop', $shopifyUser->nickname)->first();
            if(empty($token)){
              DB::table('tokens')->insert([
                'shop' => $shopifyUser->nickname,
                'oauth_token' => $shopifyUser->token
              ]);
            } else {
              DB::table('tokens')
              ->where('shop', $shopifyUser->nickname)
              ->update(array('oauth_token' => $shopifyUser->token));
            }

            $shop->users()->syncWithoutDetaching([$user->id]);
            Auth::login($user, true);
            return redirect('https://'.$shopifyUser->nickname.'/admin/apps');
        }

    }

    /**
     * Create Shopify Webhooks.
     */
    public function createShopifyWebhooks($data){
       $request = $_REQUEST;
       $server_name = $_SERVER['SERVER_NAME'];
       $script = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
       $http = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on'? "https://" : "http://";
       $base_url = $http.$_SERVER["SERVER_NAME"].$script;
       $server = $_SERVER;
       //echo "<pre>".print_r(compact('server','request','data'),true)."</pre>"; die();

       // headers.
       $headers =  array(
          'Content-Type' => 'application/json',
          'Host' => $data['shop'],
          'X-Shopify-Access-Token' => $data['oauth_token']
       );

       // delete these hooks.
       /*$dlist = "691085639779";
       foreach(explode(",",$dlist) as $d){
         $tmp = $this->shopifyRequest('DELETE', 'https://'.$data['shop'].'/admin/api/2019-07/webhooks/'.$d.'.json', [
            'headers' => $headers
         ]);
       }*/

       // get existing hooks.
       global $webhooks;
       $webhooks = $this->getShopifyHooks('GET', 'https://'.$data['shop'].'/admin/api/2019-07/webhooks.json', [
          'headers' => $headers
       ]);

       $base_url = $http.$data['shop']."/a/s/";
       //$base_url = $http."devwww.peakmarch.com/scripts/";
       //$base_url = $http."devorigin.peakmarch.com/scripts/";
       $tmp = $this->shopifyRequest('GET', 'https://'.$data['shop'].'/admin/api/2019-07/shop.json', [
          'headers' => $headers
       ]);
       //echo "<pre>".print_r($tmp,true)."</pre>"; die();
       $base_url = $http.$tmp['shop']['domain']."/a/s/";

       // create draft_orders/update hook.
       $webhook = array(
          'topic' => 'draft_orders/update',
          'address' => $base_url.'shopify_hooks/update_paypal_order.php',
          'format' => 'json',
       );
       $params = compact('webhook');
       $tmp = $this->createShopifyHook('POST', 'https://'.$data['shop'].'/admin/api/2019-07/webhooks.json', [
          'headers' => $headers,
          'body' => json_encode($params),
       ]);
       //echo "<pre>".print_r($tmp,true)."</pre>"; die();

    }

    public function createShopifyHook($method,$url,$params){
       global $webhooks;
       $tmp = json_decode($params['body'],true);
       $new = isset($tmp['webhook']) ? $tmp['webhook'] : array();
       //echo "<pre>".print_r(compact('new'),true)."</pre>"; die();
       $exists = false;
       foreach($webhooks as $wb){
         if(isset($wb['topic']) && isset($wb['address'])){
           if($wb['topic'] == $new['topic'] && $wb['address'] == $new['address']){
             $exists = true;
             break;
           }
         }
       }
       $ret = $webhooks;
       if(!$exists){
         $ret = $this->shopifyRequest($method,$url,$params);
         //echo "<pre>".print_r($tmp,true)."</pre>"; die();
       }
       return($ret);
    }

    public function getShopifyHooks($method,$url,$params){
       $tmp = $this->shopifyRequest($method,$url,$params);
       //echo "<pre>".print_r($tmp,true)."</pre>"; die();
       $ret= isset($tmp['webhooks']) ? $tmp['webhooks'] : array();
       return($ret);
    }

    public function shopifyRequest($method,$url,$params = array()){
       $client = new Client();
       $res = $client->request($method,$url,$params);
       $body = $res->getBody();
       $result = (string) $body;
       //echo "<pre>".print_r(compact('result'),true)."</pre>"; die();
       if($result){
         $result = json_decode($result,true);
       }
       return($result);
    }
}