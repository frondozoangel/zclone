<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Encryptable;

class Setting extends Model
{
    use Encryptable;

    protected $table = 'settings';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'store_id'
    ];

    protected $encryptable = [
        'live_client_id', 'live_client_secret', 'sb_client_id', 'sb_client_secret'
    ];

}