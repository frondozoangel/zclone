$(document).ready(function(){
	// things to do for btn_pp click:
	// add loading animation before ajax
	// update paypal_mode to 1(live) or 2(sandbox)
	// call polaris toast on ajax success
	$('.btn_pp').click(function(){
		$(this).addClass('Polaris-Button--primary');
		var id = $(this).attr('id');
		if (id == 'btn_pp_mode_1') {
			if ($('#btn_pp_mode_2').hasClass('Polaris-Button--primary')) {
				$('#btn_pp_mode_2').removeClass('Polaris-Button--primary');
			}
			ppDisableOtherMode('sb');
			ppEnableOtherMode('l');
		} else {
			if ($('#btn_pp_mode_1').hasClass('Polaris-Button--primary')) {
				$('#btn_pp_mode_1').removeClass('Polaris-Button--primary');
			}
			ppDisableOtherMode('l');
			ppEnableOtherMode('sb');
		}
	});

	function ppDisableOtherMode($p){
		$('.'+$p+'_tf').addClass('Polaris-TextField--disabled');
		$('.'+$p+'_tf').attr("disabled","");
		$('.'+$p+'_plrs_lt').removeClass('Polaris-Label__Text');
		$('.'+$p+'_plrs_sh').removeClass('Polaris-Subheading');
		$('.'+$p+'_plrs_lt, .'+$p+'_plrs_sh').addClass('Polaris-TextStyle--variationSubdued');
	}
	function ppEnableOtherMode($p){
		$('.'+$p+'_tf').removeClass('Polaris-TextField--disabled');
		$('.'+$p+'_tf').removeAttr("disabled");

		$('.'+$p+'_plrs_lt, .'+$p+'_plrs_sh').removeClass('Polaris-TextStyle--variationSubdued');
		if (!$('.'+$p+'_plrs_lt').hasClass('Polaris-Label__Text')) {
			$('.'+$p+'_plrs_lt').addClass('Polaris-Label__Text');
		}
		if (!$('.'+$p+'_plrs_sh').hasClass('Polaris-Subheading')) {
			$('.'+$p+'_plrs_sh').addClass('Polaris-Subheading');
		}
	}

});