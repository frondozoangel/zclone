import React, { Component } from 'react';
import '@shopify/polaris/styles.css';
import enTranslations from '@shopify/polaris/locales/en.json';
import {AppProvider, Page, Card, Button} from '@shopify/polaris';
 
/* An example React component */
class Main extends Component {
    render() {
        return (
            <div>
                <h3>All Products</h3>
            </div>
        );
    }
}
 
export default Main;
 
/* The if statement is required so as to Render the component on pages that have a div with an ID of "root";  
*/
 
if (document.getElementById('root')) {
    ReactDOM.render(
	  <AppProvider i18n={enTranslations}>
	    <Page title="Example app">
	      <Card sectioned>
	        <Button onClick={() => alert('Button clicked!')}>Example button</Button>
	      </Card>
	    </Page>
	  </AppProvider>,
	  document.querySelector('#app'),
	);
}