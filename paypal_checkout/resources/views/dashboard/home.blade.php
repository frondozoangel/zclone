@extends('layouts.master')

@section('content')
<div style="--top-bar-background:#00848e; --top-bar-color:#f9fafb; --top-bar-background-lighter:#1d9ba4;">
  <div class="Polaris-Layout">
    <div class="Polaris-Layout__AnnotatedSection">
      <div class="Polaris-Layout__AnnotationWrapper">
        <div class="Polaris-Layout__Annotation">
          <div class="Polaris-TextContainer">
            <h2 class="Polaris-Heading">Store details</h2>
            <div class="Polaris-Layout__AnnotationDescription">
              <p>Shopify and your customers will use this information to contact you.</p>
            </div>
          </div>
        </div>
        <div class="Polaris-Layout__AnnotationContent">
          <div class="Polaris-Card">
            <div class="Polaris-Card__Section">
              <div class="Polaris-FormLayout">
                <div class="Polaris-FormLayout__Item">
                  <div class="Polaris-SettingAction">
        <div class="Polaris-SettingAction__Setting">This setting is <span class="Polaris-TextStyle--variationStrong">disabled</span>.</div>
        <div class="Polaris-SettingAction__Action"><button type="button" class="Polaris-Button Polaris-Button--primary"><span class="Polaris-Button__Content"><span class="Polaris-Button__Text">Enable</span></span></button></div>
      </div>
                </div>
                <div class="Polaris-FormLayout__Item">
                  <div class="">
                    <div class="Polaris-Labelled__LabelWrapper">
                      <div class="Polaris-Label"><label id="TextField4Label" for="TextField4" class="Polaris-Label__Text">Account email</label></div>
                    </div>
                    <div class="Polaris-TextField"><input id="TextField4" class="Polaris-TextField__Input" type="email" aria-invalid="false" value="">
                      <div class="Polaris-TextField__Backdrop"></div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
