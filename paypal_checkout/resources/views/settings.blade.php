@extends('layouts.master')

@section('content')
<div style="--top-bar-background:#00848e; --top-bar-color:#f9fafb; --top-bar-background-lighter:#1d9ba4;">
  <div class="Polaris-Page">
	@if (session('error'))
	    <div class="alert alert-success">
	        {{ session('error') }}
	    </div>
	@endif
	@if (session('status'))
	    <div class="alert alert-success">
	        {{ session('status') }}
	    </div>
	@endif
    <div class="Polaris-Page-Header">
      <div class="Polaris-Page-Header__MainContent">
        <div class="Polaris-Page-Header__TitleActionMenuWrapper">
          <div>
            <div class="Polaris-Header-Title__TitleAndSubtitleWrapper">
              <div class="Polaris-Header-Title">
                <h1 class="Polaris-DisplayText Polaris-DisplayText--sizeLarge">Settings</h1>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="Polaris-Page__Content">
    	<form action="{{ route('settings.save', ['id' => $s_id]) }}" method="POST">
    	{{ csrf_field() }}
    	<div class="Polaris-Layout">
			<div class="Polaris-Layout__AnnotatedSection">
				<div class="Polaris-Layout__AnnotationWrapper">
					<div class="Polaris-Layout__Annotation">
						<div class="Polaris-TextContainer">
							<h2 class="Polaris-Heading">Global PayPal Checkout</h2>
							<div class="Polaris-Layout__AnnotationDescription">
								<p>Enable to customize PayPal checkout. Disable to default Shopify checkout.</p>
							</div>
						</div>
					</div>
					<div class="Polaris-Layout__AnnotationContent">
						<div class="Polaris-Card">
							<div class="Polaris-Card__Section">
								<div class="Polaris-SettingAction">
									<div class="Polaris-SettingAction__Setting">
										This setting is <span id="gpc-status" class="Polaris-TextStyle--variationStrong">{{ $global_status[0] }}</span>.
									</div>
									<div class="Polaris-SettingAction__Action">
										<button type="button" id="btn_gpc" data-id="{{ $global_id }}" class="Polaris-Button Polaris-Button--primary {{ $gbl_m_class }} btn_setting">
											<span class="Polaris-Button__Content"><span class="Polaris-Button__Text">{{ $global_status[1] }}</span></span>
										</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="Polaris-Layout__AnnotatedSection">
				<div class="Polaris-Layout__AnnotationWrapper">
					<div class="Polaris-Layout__Annotation">
						<div class="Polaris-TextContainer">
							<h2 class="Polaris-Heading">Force Payment Capture</h2>
							<div class="Polaris-Layout__AnnotationDescription">
								<p>Force payment capture on the shipping checkout page.</p>
							</div>
						</div>
					</div>
					<div class="Polaris-Layout__AnnotationContent">
						<div class="Polaris-Card">
							<div class="Polaris-Card__Section">
								<div class="Polaris-SettingAction">
									<div class="Polaris-SettingAction__Setting">
										This setting is <span id="fpc-status" class="Polaris-TextStyle--variationStrong">{{ $fp_status[0] }}</span>.
									</div>
									<div class="Polaris-SettingAction__Action">
										<button type="button" id="btn_fpc" data-id="{{ $fp_id }}" class="Polaris-Button {{ $fpc_m_class }} btn_setting">
											<span class="Polaris-Button__Content"><span class="Polaris-Button__Text">{{ $fp_status[1] }}</span></span>
										</button>
									</div>
								</div>
							</div>
							<div class="Polaris-Card__Section">
								<div class="">
									<div class="Polaris-Labelled__LabelWrapper">
										<div class="Polaris-Label"><label id="TextField7Label" for="TextField7" class="Polaris-Label__Text">Delay timer</label></div>
										<div class="Polaris-Labelled__Action">
											<button type="button" id="btn_change_dt" class="Polaris-Button Polaris-Button--plain btn-change">
												<span class="Polaris-Button__Content">
													<span class="Polaris-Button__Text">Change</span>
											 	</span>
											</button>
										</div>
									</div>
									<div class="Polaris-TextField Polaris-TextField--hasValue">
										<input id="delay" class="Polaris-TextField__Input Polaris-TextField--disabled" type="number" min="0" name="delay_timer"  aria-labelledby="TextField7Label TextField36Suffix" aria-invalid="false" placeholder="10" step="1" value="{{ $delay_timer }}" disabled="">
      									<div class="Polaris-TextField__Suffix" id="TextField36Suffix">minutes</div>
										<div class="Polaris-TextField__Backdrop"></div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="Polaris-Layout__AnnotatedSection">
				<div class="Polaris-Layout__AnnotationWrapper">
					<div class="Polaris-Layout__Annotation">
						<div class="Polaris-TextContainer">
							<h2 class="Polaris-Heading">Shipping</h2>
							<div class="Polaris-Layout__AnnotationDescription">
								<p>Shipping cost that will be charged with the customer's order.</p>
							</div>
						</div>
					</div>
					<div class="Polaris-Layout__AnnotationContent">
						<div class="Polaris-Card">
							<div class="Polaris-Card__Section">
								<div class="">
									<div class="Polaris-Labelled__LabelWrapper">
										<div class="Polaris-Label"><label id="TextField7Label" for="TextField7" class="Polaris-Label__Text">Cost</label></div>
										<div class="Polaris-Labelled__Action">
											<button type="button" id="btn_change_sc" class="Polaris-Button Polaris-Button--plain btn-change">
												<span class="Polaris-Button__Content">
													<span class="Polaris-Button__Text">Change</span>
											 	</span>
											</button>
										</div>
									</div>
									<div class="Polaris-TextField Polaris-TextField--hasValue">
										<div class="Polaris-TextField__Prefix" id="TextField6Prefix">$</div>
										<input id="ship_cost" class="Polaris-TextField__Input Polaris-TextField--disabled" type="number" min="0" name="shipping_cost" aria-labelledby="TextField6Label TextField7Prefix" aria-invalid="false" placeholder="0.00" step="0.01" value="{{ $shipping }}" disabled="">
										<div class="Polaris-TextField__Backdrop"></div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="Polaris-Layout__AnnotatedSection">
				<div class="Polaris-Layout__AnnotationWrapper">
					<div class="Polaris-Layout__Annotation">
						<div class="Polaris-TextContainer">
							<h2 class="Polaris-Heading">Payment</h2>
							<div class="Polaris-Layout__AnnotationDescription">
								<p>Choose Paypal mode to use during checkout.</p>
							</div>
						</div>
					</div>
					<div class="Polaris-Layout__AnnotationContent">
						<div class="Polaris-Card">
							<div class="Polaris-Card__Header">
								<svg version="1.1" x="0px" y="0px" viewBox="0 0 124 33" style="width: 100px;">
									<path fill="#253b80" d="M46.211,6.749h-6.839c-0.468,0-0.866,0.34-0.939,0.802l-2.766,17.537c-0.055,0.346,0.213,0.658,0.564,0.658 h3.265c0.468,0,0.866-0.34,0.939-0.803l0.746-4.73c0.072-0.463,0.471-0.803,0.938-0.803h2.165c4.505,0,7.105-2.18,7.784-6.5 c0.306-1.89,0.013-3.375-0.872-4.415C50.224,7.353,48.5,6.749,46.211,6.749z M47,13.154c-0.374,2.454-2.249,2.454-4.062,2.454 h-1.032l0.724-4.583c0.043-0.277,0.283-0.481,0.563-0.481h0.473c1.235,0,2.4,0,3.002,0.704C47.027,11.668,47.137,12.292,47,13.154z"></path>
									<path fill="#253b80" d="M66.654,13.075h-3.275c-0.279,0-0.52,0.204-0.563,0.481l-0.145,0.916l-0.229-0.332 c-0.709-1.029-2.29-1.373-3.868-1.373c-3.619,0-6.71,2.741-7.312,6.586c-0.313,1.918,0.132,3.752,1.22,5.031 c0.998,1.176,2.426,1.666,4.125,1.666c2.916,0,4.533-1.875,4.533-1.875l-0.146,0.91c-0.055,0.348,0.213,0.66,0.562,0.66h2.95 c0.469,0,0.865-0.34,0.939-0.803l1.77-11.209C67.271,13.388,67.004,13.075,66.654,13.075z M62.089,19.449 c-0.316,1.871-1.801,3.127-3.695,3.127c-0.951,0-1.711-0.305-2.199-0.883c-0.484-0.574-0.668-1.391-0.514-2.301 c0.295-1.855,1.805-3.152,3.67-3.152c0.93,0,1.686,0.309,2.184,0.892C62.034,17.721,62.232,18.543,62.089,19.449z"></path><path fill="#253b80" d="M84.096,13.075h-3.291c-0.314,0-0.609,0.156-0.787,0.417l-4.539,6.686l-1.924-6.425 c-0.121-0.402-0.492-0.678-0.912-0.678h-3.234c-0.393,0-0.666,0.384-0.541,0.754l3.625,10.638l-3.408,4.811 c-0.268,0.379,0.002,0.9,0.465,0.9h3.287c0.312,0,0.604-0.152,0.781-0.408L84.564,13.97C84.826,13.592,84.557,13.075,84.096,13.075z "></path>
									<path fill="#179bd7" d="M94.992,6.749h-6.84c-0.467,0-0.865,0.34-0.938,0.802l-2.766,17.537c-0.055,0.346,0.213,0.658,0.562,0.658 h3.51c0.326,0,0.605-0.238,0.656-0.562l0.785-4.971c0.072-0.463,0.471-0.803,0.938-0.803h2.164c4.506,0,7.105-2.18,7.785-6.5 c0.307-1.89,0.012-3.375-0.873-4.415C99.004,7.353,97.281,6.749,94.992,6.749z M95.781,13.154c-0.373,2.454-2.248,2.454-4.062,2.454 h-1.031l0.725-4.583c0.043-0.277,0.281-0.481,0.562-0.481h0.473c1.234,0,2.4,0,3.002,0.704 C95.809,11.668,95.918,12.292,95.781,13.154z"></path>
									<path fill="#179bd7" d="M115.434,13.075h-3.273c-0.281,0-0.52,0.204-0.562,0.481l-0.145,0.916l-0.23-0.332 c-0.709-1.029-2.289-1.373-3.867-1.373c-3.619,0-6.709,2.741-7.311,6.586c-0.312,1.918,0.131,3.752,1.219,5.031 c1,1.176,2.426,1.666,4.125,1.666c2.916,0,4.533-1.875,4.533-1.875l-0.146,0.91c-0.055,0.348,0.213,0.66,0.564,0.66h2.949 c0.467,0,0.865-0.34,0.938-0.803l1.771-11.209C116.053,13.388,115.785,13.075,115.434,13.075z M110.869,19.449 c-0.314,1.871-1.801,3.127-3.695,3.127c-0.949,0-1.711-0.305-2.199-0.883c-0.484-0.574-0.666-1.391-0.514-2.301 c0.297-1.855,1.805-3.152,3.67-3.152c0.93,0,1.686,0.309,2.184,0.892C110.816,17.721,111.014,18.543,110.869,19.449z"></path>
									<path fill="#179bd7" d="M119.295,7.23l-2.807,17.858c-0.055,0.346,0.213,0.658,0.562,0.658h2.822c0.469,0,0.867-0.34,0.939-0.803 l2.768-17.536c0.055-0.346-0.213-0.659-0.562-0.659h-3.16C119.578,6.749,119.338,6.953,119.295,7.23z"></path>
									<path fill="#253b80" d="M7.266,29.154l0.523-3.322l-1.165-0.027H1.061L4.927,1.292C4.939,1.218,4.978,1.149,5.035,1.1 c0.057-0.049,0.13-0.076,0.206-0.076h9.38c3.114,0,5.263,0.648,6.385,1.927c0.526,0.6,0.861,1.227,1.023,1.917 c0.17,0.724,0.173,1.589,0.007,2.644l-0.012,0.077v0.676l0.526,0.298c0.443,0.235,0.795,0.504,1.065,0.812 c0.45,0.513,0.741,1.165,0.864,1.938c0.127,0.795,0.085,1.741-0.123,2.812c-0.24,1.232-0.628,2.305-1.152,3.183 c-0.482,0.809-1.096,1.48-1.825,2c-0.696,0.494-1.523,0.869-2.458,1.109c-0.906,0.236-1.939,0.355-3.072,0.355h-0.73 c-0.522,0-1.029,0.188-1.427,0.525c-0.399,0.344-0.663,0.814-0.744,1.328l-0.055,0.299l-0.924,5.855l-0.042,0.215 c-0.011,0.068-0.03,0.102-0.058,0.125c-0.025,0.021-0.061,0.035-0.096,0.035H7.266z"></path>
									<path fill="#179bd7" d="M23.048,7.667L23.048,7.667L23.048,7.667c-0.028,0.179-0.06,0.362-0.096,0.55 c-1.237,6.351-5.469,8.545-10.874,8.545H9.326c-0.661,0-1.218,0.48-1.321,1.132l0,0l0,0L6.596,26.83l-0.399,2.533 c-0.067,0.428,0.263,0.814,0.695,0.814h4.881c0.578,0,1.069-0.42,1.16-0.99l0.048-0.248l0.919-5.832l0.059-0.32 c0.09-0.572,0.582-0.992,1.16-0.992h0.73c4.729,0,8.431-1.92,9.513-7.476c0.452-2.321,0.218-4.259-0.978-5.622 C24.022,8.286,23.573,7.945,23.048,7.667z"></path>
									<path fill="#222d65" d="M21.754,7.151c-0.189-0.055-0.384-0.105-0.584-0.15c-0.201-0.044-0.407-0.083-0.619-0.117 c-0.742-0.12-1.555-0.177-2.426-0.177h-7.352c-0.181,0-0.353,0.041-0.507,0.115C9.927,6.985,9.675,7.306,9.614,7.699L8.05,17.605 l-0.045,0.289c0.103-0.652,0.66-1.132,1.321-1.132h2.752c5.405,0,9.637-2.195,10.874-8.545c0.037-0.188,0.068-0.371,0.096-0.55 c-0.313-0.166-0.652-0.308-1.017-0.429C21.941,7.208,21.848,7.179,21.754,7.151z"></path>
									<path fill="#253b80" d="M9.614,7.699c0.061-0.393,0.313-0.714,0.652-0.876c0.155-0.074,0.326-0.115,0.507-0.115h7.352 c0.871,0,1.684,0.057,2.426,0.177c0.212,0.034,0.418,0.073,0.619,0.117c0.2,0.045,0.395,0.095,0.584,0.15 c0.094,0.028,0.187,0.057,0.278,0.086c0.365,0.121,0.704,0.264,1.017,0.429c0.368-2.347-0.003-3.945-1.272-5.392 C20.378,0.682,17.853,0,14.622,0h-9.38c-0.66,0-1.223,0.48-1.325,1.133L0.01,25.898c-0.077,0.49,0.301,0.932,0.795,0.932h5.791 l1.454-9.225L9.614,7.699z"></path>
								</svg>
							</div>
							<div class="Polaris-Card__Section l_section">
								<div class="Polaris-SettingAction">
									<div class="Polaris-SettingAction__Setting">
										This setting is set to <span id="pp-status" class="Polaris-TextStyle--variationStrong">{{ $pp_mode }}</span> mode.
									</div>
									<div class="Polaris-SettingAction__Action">
										<button type="button" id="btn_pp_mode" data-id="{{ $pp_id }}" class="Polaris-Button btn_setting {{ $pp_m_class }}">
											<span class="Polaris-Button__Content"><span class="Polaris-Button__Text">{{ $pp_mode }}</span></span>
										</button>
									</div>
								</div>
							</div>
							<div class="Polaris-Card__Section l_section">
								<div class="Polaris-Labelled__LabelWrapper">
									<h3 aria-label="Live" class="Polaris-Subheading l_plrs_sh">Live credentials</h3>
									<div class="Polaris-Labelled__Action">
										<button type="button" id="btn_change_live_pp" class="Polaris-Button Polaris-Button--plain btn-change">
											<span class="Polaris-Button__Content">
												<span class="Polaris-Button__Text">Change</span>
										 	</span>
										</button>
									</div>
								</div>
								<div class="">
									<div class="Polaris-Labelled__LabelWrapper">
										<div class="Polaris-Label">
											<label id="TextField1Label" for="TextField1" class="Polaris-Label__Text l_plrs_lt">Client ID</label>
										</div>
									</div>
									<div class="Polaris-TextField Polaris-TextField--hasValue">
										<input id="TextField1" class="Polaris-TextField__Input l_tf Polaris-TextField--disabled" name="live_client_id" aria-invalid="false" value="{{ decrypt($settings->live_client_id) }}" disabled="">
										<div class="Polaris-TextField__Backdrop"></div>
									</div>
								</div>
								<div class="">
									<div class="Polaris-Labelled__LabelWrapper">
										<div class="Polaris-Label">
											<label id="TextField2Label" for="TextField2" class="Polaris-Label__Text l_plrs_lt">Client Secret</label>
										</div>
									</div>
									<div class="Polaris-TextField Polaris-TextField--hasValue">
										<input id="TextField2" class="Polaris-TextField__Input l_tf Polaris-TextField--disabled" name="live_client_secret" aria-invalid="false" value="{{ decrypt($settings->live_client_secret) }}" disabled="">
										<div class="Polaris-TextField__Backdrop"></div>
									</div>
								</div>
							</div>
							<div class="Polaris-Card__Section sb_section">
								<div class="Polaris-Labelled__LabelWrapper">
									<h3 aria-label="Live" class="sb_plrs_sh Polaris-Subheading">Sandbox credentials</h3>
									<div class="Polaris-Labelled__Action">
										<button type="button" id="btn_change_sb_pp" class="Polaris-Button Polaris-Button--plain btn-change">
											<span class="Polaris-Button__Content">
												<span class="Polaris-Button__Text">Change</span>
										 	</span>
										</button>
									</div>
								</div>
								<div class="">
									<div class="Polaris-Labelled__LabelWrapper">
										<div class="Polaris-Label">
											<label id="TextField3Label" for="TextField3" class="sb_plrs_lt Polaris-Label__Text">Client ID</label>
										</div>
									</div>
									<div class="Polaris-TextField Polaris-TextField--hasValue">
										<input id="TextField3" class="Polaris-TextField__Input Polaris-TextField--disabled sb_tf" name="sb_client_id" aria-invalid="false" value="{{ decrypt($settings->sb_client_id) }}" disabled="">
										<div class="Polaris-TextField__Backdrop"></div>
									</div>
								</div>
								<div class="">
									<div class="Polaris-Labelled__LabelWrapper">
										<div class="Polaris-Label">
											<label id="TextField4Label" for="TextField4" class="sb_plrs_lt Polaris-Label__Text">Client Secret</label>
										</div>
									</div>
									<div class="Polaris-TextField Polaris-TextField--hasValue">
										<input id="TextField4" class="Polaris-TextField__Input Polaris-TextField--disabled sb_tf" name="sb_client_secret" aria-invalid="false" value="{{ decrypt($settings->sb_client_secret) }}" disabled="">
										<div class="Polaris-TextField__Backdrop"></div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="Polaris-Layout__AnnotatedSection">
				<div class="Polaris-Layout__AnnotationWrapper">
					<div class="Polaris-Layout__Annotation">
						<div class="Polaris-TextContainer">
							<h2 class="Polaris-Heading">Header script</h2>
							<div class="Polaris-Layout__AnnotationDescription">
								<p>Code used to track conversions. Google Analytics enables you to track the visitors to your store, and generates reports that will help you with your marketing. Facebook Pixel helps you create ad campaigns to find new customers that look most like your buyers.</p>
							</div>
						</div>
					</div>
					<div class="Polaris-Layout__AnnotationContent">
						<div class="Polaris-Card">
							<div class="Polaris-Card__Section">
								<div class="">
									<div class="Polaris-Labelled__LabelWrapper">
										<div class="Polaris-Label">
											<label id="TextField5Label" for="TextField5" class="Polaris-Label__Text">Script</label>
										</div>
									<div class="Polaris-Labelled__Action">
										<button type="button" id="btn_change_hs" class="Polaris-Button Polaris-Button--plain btn-change">
											<span class="Polaris-Button__Content">
												<span class="Polaris-Button__Text">Change</span>
											</span>
										</button>
									</div>
									</div>
									<div class="Polaris-TextField Polaris-TextField--hasValue Polaris-TextField--multiline">
										<textarea id="head_script" class="Polaris-TextField__Input Polaris-TextField--disabled" aria-invalid="false" aria-multiline="true" name="header_script" style="height: 100px;" disabled="">{{ $settings->header_script }}</textarea>
										<div class="Polaris-TextField__Backdrop"></div>
										<div aria-hidden="true" class="Polaris-TextField__Resizer"></div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="Polaris-Layout__AnnotatedSection">
				<div class="Polaris-Layout__AnnotationWrapper">
					<div class="Polaris-Layout__Annotation">
						<div class="Polaris-TextContainer">
							<h2 class="Polaris-Heading">Zapier/Integromat Webhook URL</h2>
							<div class="Polaris-Layout__AnnotationDescription">
								<p>Webhooks to connect draft order updates to Zapier/Integromat.</p>
							</div>
						</div>
					</div>
					<div class="Polaris-Layout__AnnotationContent">
						<div class="Polaris-Card">
							<div class="Polaris-Card__Section">
								<div class="">
									<div class="">
										<div class="Polaris-Labelled__LabelWrapper">
											<div class="Polaris-Label">
												<label id="TextField11Label" for="TextField11" class="Polaris-Label__Text">URL</label>
											</div>
											<div class="Polaris-Labelled__Action">
												<button type="button" id="btn_change_wu" class="Polaris-Button Polaris-Button--plain btn-change">
													<span class="Polaris-Button__Content">
														<span class="Polaris-Button__Text">Change</span>
													</span>
												</button>
											</div>
										</div>
										<div class="Polaris-TextField Polaris-TextField--hasValue">
											<input id="webhook_url" class="Polaris-TextField__Input Polaris-TextField--disabled wu_tf" name="webhook_uri" aria-invalid="false" value="{{ $webhook_url }}" disabled="">
											<div class="Polaris-TextField__Backdrop"></div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="Polaris-Layout__AnnotatedSection fr01D">
				<button type="submit" id="btn_save" class="Polaris-Button Polaris-Button--primary Polaris-Button--disabled" style="float:right">
					<span class="Polaris-Button__Content"><span class="Polaris-Button__Text">Save</span></span>
				</button>
			</div>
		</div>
		</form>
    </div>
  </div>
</div>
@endsection

@section('scripts')
    <script type="text/javascript">
    	$(document).ready(function(){
			// things to do for btn_pp click:
			// add loading animation before ajax
			// update paypal_mode to 1(live) or 2(sandbox)
			// call polaris toast on ajax success
			$('.btn_setting').click(function(){
				var btn_id = $(this).attr('id');
				var dataid = $(this).data("id");
				var data;
				var d;
				var url;
				if (btn_id == 'btn_pp_mode') {
					url = '/home/settings/change_paypal_mode';
					(dataid == 1) ? d=2 : d=1;
				} else if (btn_id == "btn_gpc") {
					url = '/home/settings/update/gpc';
					(dataid == 1) ? d=0 : d=1;
				} else {
					url = '/home/settings/update/fpc';
					(dataid == 1) ? d=0 : d=1;
				}
				data = {mode:d,id:'{{$s_id}}'};
				var btn = $(this);
				ajaxPost(url,data,btn,btn_id);
			});
			$('#btn_change_live_pp').click(function(){
				$('.l_tf').removeClass('Polaris-TextField--disabled');
				$('.l_tf').removeAttr("disabled");
			});
			$('#btn_change_sb_pp').click(function(){
				$('.sb_tf').removeClass('Polaris-TextField--disabled');
				$('.sb_tf').removeAttr("disabled");
			});
			$('#btn_change_sc').click(function(){
				$('#ship_cost').removeClass('Polaris-TextField--disabled');
				$('#ship_cost').removeAttr("disabled");
			});
			$('#btn_change_hs').click(function(){
				$('#head_script').removeClass('Polaris-TextField--disabled');
				$('#head_script').removeAttr("disabled");
			});
			$('#btn_change_wu').click(function(){
				$('#webhook_url').removeClass('Polaris-TextField--disabled');
				$('#webhook_url').removeAttr("disabled");
			});
			$('#btn_change_dt').click(function(){
				$('#delay').removeClass('Polaris-TextField--disabled');
				$('#delay').removeAttr("disabled");
			});
			$('input,textarea').keyup(function(){
				$('#btn_save').removeClass('Polaris-Button--disabled');
			});
			function ajaxPost(url,data,btn,btnid){
				$.ajaxSetup({
					headers: {
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					}
				});
				$.ajax({
					url: url,
					method: 'POST',
					data: data,
					beforeSend: function() {
						btnShowSpinner(btn,btnid);
					},
					success: function(response){
						var obj = JSON.parse(response);
						console.log(obj);
						if (obj.status == 200) {
							btnRemoveSpinner(btn);
							if (btnid == 'btn_pp_mode') {
								ppStatus(obj.mode);
							} else if (btnid == 'btn_gpc') {
								gpcStatus(obj.mode);
							} else {
								fpcStatus(obj.mode);
							}
						} else {

						}
					}
				});
			}
			function btnShowSpinner(btn,btnid){
				btn.addClass('Polaris-Button--disabled Polaris-Button--loading');
				btn.attr("disabled", "");
				btn.attr("aria-busy", "true");
				btn.attr("role", "alert");
				btn.find('Polaris-Button__Content');
				$('#'+btnid+' .Polaris-Button__Content').append('<span id="pp_mode_spinner" class="Polaris-Button__Spinner"><img id="pp_mode_spinner_img" src="data:image/svg+xml;base64,PHN2ZyB2aWV3Qm94PSIwIDAgMjAgMjAiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+PHBhdGggZD0iTTcuMjI5IDEuMTczYTkuMjUgOS4yNSAwIDEgMCAxMS42NTUgMTEuNDEyIDEuMjUgMS4yNSAwIDEgMC0yLjQtLjY5OCA2Ljc1IDYuNzUgMCAxIDEtOC41MDYtOC4zMjkgMS4yNSAxLjI1IDAgMSAwLS43NS0yLjM4NXoiIGZpbGw9IiM5MTlFQUIiLz48L3N2Zz4K" alt="" class="Polaris-Spinner Polaris-Spinner--colorInkLightest Polaris-Spinner--sizeSmall" draggable="false" role="status" aria-label="Loading"></span>');
			}
			function btnRemoveSpinner(btn){
				btn.removeClass('Polaris-Button--disabled Polaris-Button--loading');
				btn.removeAttr("disabled", "");
				btn.removeAttr("aria-busy", "true");
				btn.removeAttr("role", "alert");
				$('#pp_mode_spinner').remove();
				$('#pp_mode_spinner_img').remove();
			}
			function ppStatus(mode){
				$('#btn_pp_mode').find(".Polaris-Button__Text").html(mode[0]);
				$("#btn_pp_mode").data("id",mode[1]);
				$("#pp-status").html(mode[0]);
				if (mode[1] != 1) {
					$('#btn_pp_mode').removeClass('Polaris-Button--primary');
				} else {
					$('#btn_pp_mode').addClass('Polaris-Button--primary');
				}
			}
			function gpcStatus(mode){
				$('#btn_gpc').find(".Polaris-Button__Text").html(mode[0][1]);
				$("#btn_gpc").data("id",mode[1]);
				$("#gpc-status").html(mode[0][0]);
				if (mode[1] == 1) {
					$('#btn_gpc').removeClass('Polaris-Button--primary');
				} else {
					$('#btn_gpc').addClass('Polaris-Button--primary');
				}
			}
			function fpcStatus(mode){
				$('#btn_fpc').find(".Polaris-Button__Text").html(mode[0][1]);
				$("#btn_fpc").data("id",mode[1]);
				$("#fpc-status").html(mode[0][0]);
				if (mode[1] == 1) {
					$('#btn_fpc').removeClass('Polaris-Button--primary');
				} else {
					$('#btn_fpc').addClass('Polaris-Button--primary');
				}
			}
			/*function showToast(msg){
				var p = document.createElement("div");
				p.classList.add("Polaris-Frame-ToastManager");
				p.setAttribute("aria-live", "polite");
				var c1 = document.createElement("div");
				p.classList.add('Polaris-Frame-ToastManager__ToastWrapper');
				p.classList.add('Polaris-Frame-ToastManager--toastWrapperEnterDone');
				$('body').append(p);
				$('.Polaris-Frame-ToastManager').css("--toast-translate-y-in", "-80px");
				$('.Polaris-Frame-ToastManager').css("--toast-translate-y-out","70");
				var c2 = document.createElement('div');
				c2.classList.add('Polaris-Frame-Toast');
				c2.innerHTML = msg;
				var b = document.createElement("button");
				b.setAttribute("type", "button");
				b.classList.add("Polaris-Frame-Toast__CloseButton");
				var s = document.createElement("span");
				s.classList.add("Polaris-Icon");
				var svg = document.createElement("svg");
				svg.classList.add("Polaris-Icon__Svg");
				svg.setAttribute("viewBox", "0 0 20 20");
				svg.setAttribute("focusable", "false");
				svg.setAttribute("aria-hidden", "true");
				var pt = document.createElement("path");
				pt.setAttribute("d", "M11.414 10l6.293-6.293a.999.999 0 1 0-1.414-1.414L10 8.586 3.707 2.293a.999.999 0 1 0-1.414 1.414L8.586 10l-6.293 6.293a.999.999 0 1 0 1.414 1.414L10 11.414l6.293 6.293a.997.997 0 0 0 1.414 0 .999.999 0 0 0 0-1.414L11.414 10z");
				pt.setAttribute("fill-rule", "evenodd");
				p.append(c1);
				c1.append(c2);
				c2.append(b);
				b.append(s);
				s.append(svg);
				svg.append(pt);
			}*/
		});
    </script>
@endsection