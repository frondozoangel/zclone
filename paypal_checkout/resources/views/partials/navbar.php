<div>
  <ul role="tablist" class="Polaris-Tabs">
    <li class="Polaris-Tabs__TabContainer"><button id="all-customers" role="tab" type="button" tabindex="0" class="Polaris-Tabs__Tab Polaris-Tabs__Tab--selected" aria-selected="true" aria-controls="settingsPanel"><span class="Polaris-Tabs__Title">Settings</span></button></li>
    <li class="Polaris-Tabs__TabContainer"><button id="accepts-marketing" role="tab" type="button" tabindex="-1" class="Polaris-Tabs__Tab" aria-selected="false" aria-controls="globalPanel"><span class="Polaris-Tabs__Title">Global</span></button></li>
    <li class="Polaris-Tabs__DisclosureTab">
      <div><button type="button" class="Polaris-Tabs__DisclosureActivator" aria-label="More tabs" tabindex="0" aria-controls="Popover57" aria-owns="Popover57" aria-haspopup="true" aria-expanded="false"><span class="Polaris-Tabs__Title"><span class="Polaris-Icon"><svg viewBox="0 0 20 20" class="Polaris-Icon__Svg" focusable="false" aria-hidden="true">
                <path d="M6 10a2 2 0 1 1-4.001-.001A2 2 0 0 1 6 10zm6 0a2 2 0 1 1-4.001-.001A2 2 0 0 1 12 10zm6 0a2 2 0 1 1-4.001-.001A2 2 0 0 1 18 10z" fill-rule="evenodd"></path>
              </svg></span></span></button></div>
    </li>
  </ul>
  <div class="Polaris-Tabs Polaris-Tabs__TabMeasurer">
    <li class="Polaris-Tabs__TabContainer"><button id="all-customersMeasurer" role="tab" type="button" tabindex="-1" class="Polaris-Tabs__Tab Polaris-Tabs__Tab--selected" aria-selected="true"><span class="Polaris-Tabs__Title">Settings</span></button></li>
    <li class="Polaris-Tabs__TabContainer"><button id="accepts-marketingMeasurer" role="tab" type="button" tabindex="-1" class="Polaris-Tabs__Tab" aria-selected="false"><span class="Polaris-Tabs__Title">Global</span></button></li><button type="button" class="Polaris-Tabs__DisclosureActivator" aria-label="More tabs"><span class="Polaris-Tabs__Title"><span class="Polaris-Icon"><svg viewBox="0 0 20 20" class="Polaris-Icon__Svg" focusable="false" aria-hidden="true">
            <path d="M6 10a2 2 0 1 1-4.001-.001A2 2 0 0 1 6 10zm6 0a2 2 0 1 1-4.001-.001A2 2 0 0 1 12 10zm6 0a2 2 0 1 1-4.001-.001A2 2 0 0 1 18 10z" fill-rule="evenodd"></path>
          </svg></span></span></button>
  </div>
  <div class="Polaris-Tabs__Panel Polaris-Tabs__Panel--hidden" id="globalPanel" role="tabpanel" aria-labelledby="accepts-marketing" tabindex="-1"></div>
</div>