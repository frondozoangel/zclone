<!DOCTYPE html>
<html>
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://unpkg.com/@shopify/polaris@4.1.0/styles.min.css"/>
    <link rel="stylesheet" href="{{ url('/assets/css/styles.css') }}">
    <title>@yield('title')</title>
</head>
<body>
    <!-- @include('partials.navbar') -->
    @yield('content')

    <script src="{{  url('assets/js/jquery-3.4.1.min.js') }}"></script>
    @yield('scripts')
</body>
</html>