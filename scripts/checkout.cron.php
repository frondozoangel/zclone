<?php
  header('Access-Control-Allow-Origin: *');
  error_reporting(E_ALL);
  ini_set("display_errors", 1);
  //phpinfo();
  $data = $_REQUEST;
  //echo "<pre>".print_r($data,true)."</pre>"; //die();

  require_once("functions.v2.php");
  require_once("db.php");
  require_once("sfunctions.php");

  if(isset($_GET['token'])){
    $incomplete = getRows("SELECT store, paypal_token token, shopify_draft_order_id shopify_checkout_token, cookies FROM checkouts WHERE paypal_token = '".$_GET['token']."' AND paypal_token <> '' ");
  } else {
    //echo "<pre>".print_r(compact('delay_timer'),true)."</pre>";
    $sql = "SELECT store, paypal_token token, shopify_draft_order_id shopify_checkout_token, cookies, TIMESTAMPDIFF(MINUTE,created,NOW()) age FROM checkouts WHERE shopify_draft_order_id IS NOT NULL AND completed IS NULL ";
    echo "<br>".$sql."<br>";
    $incomplete = getRows($sql);
  }
  echo "<pre>".print_r(compact('incomplete'),true)."</pre>";
  foreach($incomplete as $row){
    $data['shop_subdomain'] = $row['store'];
    get_paypal_settings();
    $age = $row['age'];
    $token = $row['token'];
    $store = $row['store'];
    echo "<pre>".print_r(compact('token','store','force_payment_capture','delay_timer','age'),true)."</pre>";
    //echo "<pre>".print_r(compact('row','settings'),true)."</pre>";
    if($force_payment_capture && $age > $delay_timer || isset($_GET['token'])){
      complete_purchase($row);
    }
  }

  function complete_purchase($data){
    global $access_token,$get_order_url,$client_id,$client_secret,$store;
    //echo "<pre>".print_r($data,true)."</pre>"; die();

    $access_token = get_access_token();
    //echo "<pre>".print_r(compact('access_token'),true)."</pre>"; die();

    // process paypal payment.
    $tmp2 = capture_paypal_payment($data['token']);
    //echo "<pre>".print_r($tmp2,true)."</pre>";
    $capture_id = "invalid capture link";
    $capture_status = 'INVALID';
    if(isset($tmp2['purchase_units'])){
      if(isset($tmp2['purchase_units'][0])){
        if(isset($tmp2['purchase_units'][0]['payments'])){
          if(isset($tmp2['purchase_units'][0]['payments']['captures'])){
            if(isset($tmp2['purchase_units'][0]['payments']['captures'][0])){
              if(isset($tmp2['purchase_units'][0]['payments']['captures'][0]['id'])){
                $capture_id = $tmp2['purchase_units'][0]['payments']['captures'][0]['id'];
                $capture_status = $tmp2['purchase_units'][0]['payments']['captures'][0]['status'];
              }
            }
          }
        }
      }
    } else {
      $tmp2 = get_paypal_order($data['token']);
      if(isset($tmp2['purchase_units'])){
        if(isset($tmp2['purchase_units'][0])){
          if(isset($tmp2['purchase_units'][0]['payments'])){
            if(isset($tmp2['purchase_units'][0]['payments']['captures'])){
              if(isset($tmp2['purchase_units'][0]['payments']['captures'][0])){
                if(isset($tmp2['purchase_units'][0]['payments']['captures'][0]['id'])){
                  $capture_id = $tmp2['purchase_units'][0]['payments']['captures'][0]['id'];
                  $capture_status = $tmp2['purchase_units'][0]['payments']['captures'][0]['status'];
                }
              }
            }
          }
        }
      }
    }

    // update draft order in db.
    $paypal_order = str_replace("'","\'",json_encode($tmp2));
    $sql = "UPDATE checkouts SET ";
    $sql .= "status = 'completed' ";
    $sql .= ", completed = NOW(), completed_by = 'cron' ";
    $sql .= ", paypal_capture_id = '".$capture_id."' ";
    $sql .= ", paypal_order = '".$paypal_order."' ";
    $sql .= "WHERE paypal_token = '".$data['token']."' ";
    echo "<br>".$sql."<br>";
    $tmp = query($sql);

    // add tags and meta data to draft order.
    $note_attributes = array();
    $note_attributes[] = json_decode('{
            "name": "paypal_capture_id",
            "value": "'.$capture_id.'"
          }',true);
    $note_attributes[] = json_decode('{
            "name": "paypal_capture_status",
            "value": "'.$capture_status.'"
          }',true);

    $store = $data['store'];
    $cookies = urldecode($data['cookies']);
    //echo "<pre>".print_r($cookies,true)."</pre>"; die();
    $rows = explode(";",$cookies);
    foreach($rows as $row){
      $cparts = explode("=",$row);
      $remove_list = "_ab,_shopify_y,_gcl_au,_y,_shopify_fs,_fbp,_ga,_gid,_s,_shopify_s,_shopify_sa_p,shopify_pay_redirect,_shopify_sa_t";
      $name = trim($cparts[0]);
      $value = trim($cparts[1]);
      if(!in_array($name,explode(",",$remove_list))){
        $note_attributes[] = json_decode('{
              "name": "'.$name.'",
              "value": "'.$value.'"
            }',true);
      }
    }

    // get shopify draft order.
    $tmp2 = get_shopify_draft_order($data['shopify_checkout_token']);
    //echo "<pre>".print_r($tmp2,true)."</pre>";

    // get shopify order.
    if(isset($tmp2['order_id'])){
      $shopify_order = get_shopify_order($tmp2['order_id']);
    } else {
      // complete shopify order.
      $tmp3 = complete_shopify_draft_order($data['shopify_checkout_token']);
      //echo "<pre>".print_r($tmp3,true)."</pre>"; //die();
      $shopify_order = get_shopify_order($tmp3['order_id']);
    }
    $tmp4 = $shopify_order;

    // update notes attributes and tags.
    if(isset($shopify_order['id'])){
      $json = '{
        "order": {
          "id": '.$shopify_order['id'].',
          "tags": "forced capture",
          "note_attributes": '.json_encode($note_attributes).'
        }
      }';
      $tmp4 = update_shopify_order($shopify_order['id'],$json);
    }

    // cancel the order if payment is declined or invalid.
    if($capture_status == 'DECLINED' || $capture_status == 'INVALID'){
      $json = '{
        "reason": "'.($capture_status == 'DECLINED' ? 'declined' : 'other').'"
      }';
      $tmp4 = cancel_shopify_order($shopify_order['id'],$json);
    }

    // update draft order in db.
    $sql = "UPDATE checkouts SET ";
    $sql .= "shopify_order = '".json_encode($tmp4)."' ";
    $sql .= "WHERE paypal_token = '".$data['token']."' ";
    echo "<br>".$sql."<br>";
    $tmp = query($sql);

  }

?>

