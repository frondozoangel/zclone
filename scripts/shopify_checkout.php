<!DOCTYPE html>
<!-- saved from url=(0014)about:internet -->
<html lang="en" dir="ltr" class="js windows chrome desktop page--no-banner page--logo-main page--show page--show card-fields cors svg opacity placeholder no-touchevents displaytable display-table generatedcontent cssanimations flexbox no-flexboxtweener anyflexbox no-shopemoji floating-labels" style="">
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
      <meta name="viewport" content="width=device-width, initial-scale=1.0, height=device-height, minimum-scale=1.0, user-scalable=0">
      <meta name="referrer" content="origin">
      <title>Complete Your Purchase</title>
      <meta data-browser="chrome" data-browser-major="76">
      <meta data-body-font-family="-apple-system, BlinkMacSystemFont, &#39;Segoe UI&#39;, Roboto, Helvetica, Arial, sans-serif, &#39;Apple Color Emoji&#39;, &#39;Segoe UI Emoji&#39;, &#39;Segoe UI Symbol&#39;" data-body-font-type="system">
      <meta id="shopify-digital-wallet" name="shopify-digital-wallet" content="/27022032995/digital_wallets/dialog">
      <meta name="shopify-checkout-api-token" content="8b1babdba891d26592c00b64775d455c">
      <meta name="shopify-checkout-authorization-token" content="138450adece544827143baa6a213715d">
      <meta id="shopify-regional-checkout-config" name="shopify-regional-checkout-config" content="{&quot;bugsnag&quot;:{&quot;checkoutJSApiKey&quot;:&quot;717bcb19cf4dd1ab6465afcec8a8de02&quot;,&quot;endpoint&quot;:&quot;https:\/\/notify.bugsnag.com&quot;}}">
      <meta id="autocomplete-service-sandbox-url" name="autocomplete-service-sandbox-url" content="https://checkout.shopify.com/27022032995/sandbox/autocomplete_service?locale=en">
      <!--[if lt IE 9]>
      <link rel="stylesheet" media="all" href="//cdn.shopify.com/app/services/27022032995/assets/76321685603/checkout_stylesheet/v2-ltr-edge-73ed7b0559a2758cc4e4bf7e7ef532f1-148/oldie" />
      <![endif]-->
      <!--[if gte IE 9]><!-->
      <link rel="stylesheet" type="text/css" media="all" href="/a/s/checkout_files/style.css">
      <!--<![endif]-->
      <style type="text/css">
         #continue_button {
            background-color: green;
         }
         #continue_button:hover {
            background-color: #006400;
         }
         /*@keyframes spinner {
           to {transform: rotate(360deg);}
         }
         .spinner:before {
           content: '';
           box-sizing: border-box;
           position: absolute;
           top: 50%;
           left: 50%;
           width: 20px;
           height: 20px;
           margin-top: -10px;
           margin-left: -10px;
           border-radius: 50%;
           border: 2px solid #ccc;
           border-top-color: #000;
           animation: spinner .6s linear infinite;
         }*/
      </style>
      <?php
        require_once("checkout_header.php");
      ?>
   </head>
   <body>
      <a href="https://mm-teststore2.myshopify.com/27022032995/checkouts/5d46647e5e070cdbaf0584050287efc3#main-header" class="skip-to-content">
      Skip to content
      </a>
      <div class="banner" data-header="">
         <div class="wrap">
            <span class="logo logo--left"><span class="logo__text heading-1"><?php echo $domain; ?></span></span>
            <h1 class="visually-hidden">
               Shipping
            </h1>
         </div>
      </div>
      <button class="order-summary-toggle order-summary-toggle--show shown-if-js" data-trekkie-id="order_summary_toggle" aria-expanded="false" aria-controls="order-summary" data-drawer-toggle="[data-order-summary]">
         <span class="wrap">
            <span class="order-summary-toggle__inner">
               <span class="order-summary-toggle__icon-wrapper">
                  <svg width="20" height="19" xmlns="http://www.w3.org/2000/svg" class="order-summary-toggle__icon">
                     <path d="M17.178 13.088H5.453c-.454 0-.91-.364-.91-.818L3.727 1.818H0V0h4.544c.455 0 .91.364.91.818l.09 1.272h13.45c.274 0 .547.09.73.364.18.182.27.454.18.727l-1.817 9.18c-.09.455-.455.728-.91.728zM6.27 11.27h10.09l1.454-7.362H5.634l.637 7.362zm.092 7.715c1.004 0 1.818-.813 1.818-1.817s-.814-1.818-1.818-1.818-1.818.814-1.818 1.818.814 1.817 1.818 1.817zm9.18 0c1.004 0 1.817-.813 1.817-1.817s-.814-1.818-1.818-1.818-1.818.814-1.818 1.818.814 1.817 1.818 1.817z"></path>
                  </svg>
               </span>
               <span class="order-summary-toggle__text order-summary-toggle__text--show">
                  <span>Show order summary</span>
                  <svg width="11" height="6" xmlns="http://www.w3.org/2000/svg" class="order-summary-toggle__dropdown" fill="#000">
                     <path d="M.504 1.813l4.358 3.845.496.438.496-.438 4.642-4.096L9.504.438 4.862 4.534h.992L1.496.69.504 1.812z"></path>
                  </svg>
               </span>
               <span class="order-summary-toggle__text order-summary-toggle__text--hide">
                  <span>Hide order summary</span>
                  <svg width="11" height="7" xmlns="http://www.w3.org/2000/svg" class="order-summary-toggle__dropdown" fill="#000">
                     <path d="M6.138.876L5.642.438l-.496.438L.504 4.972l.992 1.124L6.138 2l-.496.436 3.862 3.408.992-1.122L6.138.876z"></path>
                  </svg>
               </span>
               <span class="order-summary-toggle__total-recap total-recap" data-order-summary-section="toggle-total-recap">
               <span class="total-recap__final-price" data-checkout-payment-due-target="1950">$<?php echo ($subtotal_price + 4.95);?></span>
               </span>
            </span>
         </span>
      </button>
      <div class="content" data-content="">
         <div class="wrap">
            <div class="main" role="main">
               <div class="main__header">
                  <span class="logo logo--left"><span class="logo__text heading-1"><?php echo $domain; ?></span></span>
                  <h1 class="visually-hidden">
                     Shipping
                  </h1>
                  <nav aria-label="Breadcrumb" style="display:none">
                     <ul class="breadcrumb ">
                        <li class="breadcrumb__item breadcrumb__item--completed">
                           <a class="breadcrumb__link" data-trekkie-id="breadcrumb_contact_information_link" href="https://mm-teststore2.myshopify.com/27022032995/checkouts/5d46647e5e070cdbaf0584050287efc3?step=contact_information">Information</a>
                           <svg class="icon-svg icon-svg--color-adaptive-light icon-svg--size-10 breadcrumb__chevron-icon" aria-hidden="true" focusable="false">
                              <use xlink:href="#chevron-right"></use>
                           </svg>
                        </li>
                        <li class="breadcrumb__item breadcrumb__item--current">
                           <span class="breadcrumb__text" aria-current="step">Shipping</span>
                           <svg class="icon-svg icon-svg--color-adaptive-light icon-svg--size-10 breadcrumb__chevron-icon" aria-hidden="true" focusable="false">
                              <use xlink:href="#chevron-right"></use>
                           </svg>
                        </li>
                        <li class="breadcrumb__item breadcrumb__item--blank">
                           <span class="breadcrumb__text">Payment</span>
                        </li>
                     </ul>
                  </nav>
                  <div class="shown-if-js" data-alternative-payments="">
                  </div>
               </div>
               <div class="main__content">
                  <!--<iframe src="./checkout_files/google_analytics_iframe.html" onload="this.setAttribute(&#39;data-loaded&#39;, true)" sandbox="allow-scripts" id="google-analytics-sandbox" tabindex="-1" class="visually-hidden" style="display:none" aria-hidden="true" data-loaded="true"></iframe>-->
                  <div class="step" data-step="shipping_method" data-last-step="false">
                     <form class="edit_checkout animate-floating-labels" data-shipping-method-form="true" action="https://mm-teststore2.myshopify.com/27022032995/checkouts/5d46647e5e070cdbaf0584050287efc3" accept-charset="UTF-8" method="post">
                        <input type="hidden" name="_method" value="patch"><input type="hidden" name="authenticity_token" value="HMsVCCl6Qksc5ZYU6GZjU8yJVhMZNRIKyaqupnJwu3XzbDTFvDLqXQUkl3HFVLMB96YU4yT1+rqYIIHNnWaBYQ==">
                        <input type="hidden" name="previous_step" id="previous_step" value="shipping_method">
                        <input type="hidden" name="step" value="payment_method">
                        <div class="step__sections">
                           <div class="section">
                              <div class="content-box">
                                 <div role="grid" class="content-box__row content-box__row--tight-spacing-vertical">
                                    <div role="row" class="review-block">
                                       <div class="review-block__inner">
                                          <div role="gridcell" class="review-block__label">
                                             Contact
                                          </div>
                                          <div role="gridcell" class="review-block__content">
                                             <bdo dir="ltr"><?php echo $payer['email_address']; ?></bdo>
                                          </div>
                                       </div>
                                       <div role="gridcell" class="review-block__link hidden">
                                          <a data-trekkie-id="change_contact_method_link" class="link--small" href="https://mm-teststore2.myshopify.com/27022032995/checkouts/5d46647e5e070cdbaf0584050287efc3?step=contact_information">
                                          <span aria-hidden="true">Change</span>
                                          <span class="visually-hidden">Change contact information</span>
                                          </a>
                                       </div>
                                    </div>
                                    <div role="row" class="review-block">
                                       <div class="review-block__inner">
                                          <div role="gridcell" class="review-block__label">
                                             Ship to
                                          </div>
                                          <div role="gridcell" class="review-block__content">
                                             <address class="address address--tight">
                                                <?php echo $address1; ?>, <?php echo $city; ?> <?php echo $state; ?> <?php echo $zip; ?>, <?php echo $country; ?>
                                             <address>
                                             </address>
                                             </address>
                                          </div>
                                       </div>
                                       <div role="gridcell" class="review-block__link hidden">
                                          <a class="link--small" data-trekkie-id="change_shipping_address_link" href="https://mm-teststore2.myshopify.com/27022032995/checkouts/5d46647e5e070cdbaf0584050287efc3?step=contact_information">
                                          <span aria-hidden="true">Change</span>
                                          <span class="visually-hidden">Change shipping address</span>
                                          </a>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="fieldset-description" data-buyer-accepts-marketing="">
                                 <div class="section__content">
                                    <div class="checkbox-wrapper">
                                       <div class="checkbox__input">
                                          <input name="checkout[buyer_accepts_marketing]" type="hidden" value="0"><input class="input-checkbox" data-backup="buyer_accepts_marketing" data-trekkie-id="buyer_accepts_marketing_field" type="checkbox" value="1" name="checkout[buyer_accepts_marketing]" id="checkout_buyer_accepts_marketing" checked>
                                       </div>
                                       <label class="checkbox__label" for="checkout_buyer_accepts_marketing">
                                       Keep me up to date on news and exclusive offers
                                       </label>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="section section--shipping-method">
                              <div class="section__header">
                                 <h2 class="section__title" id="main-header" tabindex="-1">
                                    Shipping method
                                 </h2>
                              </div>
                              <div class="section__content">
                                 <fieldset class="content-box" data-shipping-methods="">
                                    <legend class="visually-hidden">Choose a shipping method</legend>
                                    <div class="content-box__row">
                                       <div class="radio-wrapper" data-shipping-method="usps-FirstClassPackageInternationalService-9.50">
                                          <div class="radio__input">
                                             <input class="input-radio" data-checkout-total-shipping="$9.50" data-checkout-total-shipping-cents="950" data-checkout-shipping-rate="$9.50" data-checkout-original-shipping-rate="$9.50" data-checkout-total-taxes="$0.00" data-checkout-total-taxes-cents="0" data-checkout-total-price="$19.50" data-checkout-total-price-cents="1950" data-checkout-payment-due="$19.50" data-checkout-payment-due-cents="1950" data-checkout-payment-subform="required" data-checkout-subtotal-price="$10.00" data-checkout-subtotal-price-cents="1000" data-backup="usps-FirstClassPackageInternationalService-9.50" aria-label="USPS First Class Package International. 7 to 21 business days. $9.50" type="radio" value="usps-FirstClassPackageInternationalService-9.50" name="checkout[shipping_rate][id]" id="checkout_shipping_rate_id_usps-firstclasspackageinternationalservice-9_50" checked="checked">
                                          </div>
                                          <label class="radio__label" aria-hidden="true" for="checkout_shipping_rate_id_usps-firstclasspackageinternationalservice-9_50">
                                          <span class="radio__label__primary" data-shipping-method-label-title="First Class Package International">
                                          Standard Shipping
                                          <br>
                                          <!--<span class="small-text">7 to 21 business days</span>-->
                                          </span>
                                          <span class="radio__label__accessory">
                                          <span class="content-box__emphasis">
                                          $<?php echo $shipping_cost; ?>
                                          </span>
                                          </span>
                                          </label>
                                       </div>
                                       <!-- /radio-wrapper-->
                                    </div>
                                 </fieldset>
                              </div>
                           </div>
                        </div>
                        <div class="step__footer" data-step-footer="">
                           <button name="button" type="submit" id="continue_button" style="background-color:green" class="step__footer__continue-btn btn " aria-busy="false" trekkie_id="continue_to_payment_method_button">
                              <span class="btn__content">Complete Purchase</span>
                              <svg class="icon-svg icon-svg--size-18 btn__spinner icon-svg--spinner-button" aria-hidden="true" focusable="false">
                                 <use xlink:href="#spinner-button"></use>
                              </svg>
                           </button>
                           <a class="step__footer__previous-link" style="display:none" data-trekkie-id="previous_step_link" href="https://mm-teststore2.myshopify.com/27022032995/checkouts/5d46647e5e070cdbaf0584050287efc3?step=contact_information">
                              <svg focusable="false" aria-hidden="true" class="icon-svg icon-svg--color-accent icon-svg--size-10 previous-link__icon" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 10 10">
                                 <path d="M8 1L7 0 3 4 2 5l1 1 4 4 1-1-4-4"></path>
                              </svg>
                              <span class="step__footer__previous-link-content">Return to information</span>
                           </a>
                        </div>
                        <input type="hidden" name="checkout[client_details][browser_width]" value="1349"><input type="hidden" name="checkout[client_details][browser_height]" value="625"><input type="hidden" name="checkout[client_details][javascript_enabled]" value="1"><input type="hidden" name="checkout[client_details][color_depth]" value="24"><input type="hidden" name="checkout[client_details][java_enabled]" value="false"><input type="hidden" name="checkout[client_details][browser_tz]" value="-480">
                     </form>
                  </div>
                  <span class="visually-hidden" id="forwarding-external-new-window-message">
                  Opens external website in a new window.
                  </span>
                  <span class="visually-hidden" id="forwarding-new-window-message">
                  Opens in a new window.
                  </span>
                  <span class="visually-hidden" id="forwarding-external-message">
                  Opens external website.
                  </span>
                  <span class="visually-hidden" id="checkout-context-step-one">
                  Customer information - mm-teststore2 - Checkout
                  </span>
               </div>
               <div class="main__footer">
                  <div role="contentinfo" aria-label="Footer">
                     <p class="copyright-text">
                        All rights reserved <?php echo $domain; ?>
                     </p>
                  </div>
               </div>
            </div>
            <div class="sidebar" role="complementary">
               <div class="sidebar__header">
                  <span class="logo logo--left"><span class="logo__text heading-1">mm-teststore2</span></span>
                  <h1 class="visually-hidden">
                     Shipping
                  </h1>
               </div>
               <div class="sidebar__content">
                  <div id="order-summary" class="order-summary order-summary--is-collapsed" data-order-summary="">
                     <h2 class="visually-hidden-if-js">Order summary</h2>
                     <div class="order-summary__sections">
                        <div class="order-summary__section order-summary__section--product-list">
                           <div class="order-summary__section__content">
                              <table class="product-table">
                                 <caption class="visually-hidden">Shopping cart</caption>
                                 <thead class="product-table__header">
                                    <tr>
                                       <th scope="col"><span class="visually-hidden">Product image</span></th>
                                       <th scope="col"><span class="visually-hidden">Description</span></th>
                                       <th scope="col"><span class="visually-hidden">Quantity</span></th>
                                       <th scope="col"><span class="visually-hidden">Price</span></th>
                                    </tr>
                                 </thead>
                                 <tbody data-order-summary-section="line-items">
                                    <?php foreach($line_items as $line_item){ ?>
                                    <tr class="product" data-product-id="<?php echo $line_item['product_id'];?>" data-variant-id="<?php echo $line_item['variant_id'];?>" data-product-type="" data-customer-ready-visible="">
                                       <td class="product__image">
                                          <div class="product-thumbnail ">
                                             <div class="product-thumbnail__wrapper">
                                                <img alt="<?php echo $line_item['title'];?>" class="product-thumbnail__image" src="<?php echo $line_item['image_url'];?>">
                                             </div>
                                             <span class="product-thumbnail__quantity" aria-hidden="true"><?php echo $line_item['quantity'];?></span>
                                          </div>
                                       </td>
                                       <th class="product__description" scope="row">
                                          <span class="product__description__name order-summary__emphasis"><?php echo $line_item['title'];?></span>
                                          <span class="product__description__variant order-summary__small-text"></span>
                                       </th>
                                       <td class="product__quantity visually-hidden">
                                          1
                                       </td>
                                       <td class="product__price">
                                          <span class="order-summary__emphasis">$<?php echo $line_item['price'];?></span>
                                       </td>
                                    </tr>
                                    <?php } ?>
                                 </tbody>
                              </table>
                              <div class="order-summary__scroll-indicator" aria-hidden="true" tabindex="-1">
                                 Scroll for more items
                                 <svg aria-hidden="true" focusable="false" class="icon-svg icon-svg--size-12">
                                    <use xlink:href="#down-arrow"></use>
                                 </svg>
                              </div>
                           </div>
                        </div>
                        <div class="order-summary__section order-summary__section--total-lines" data-order-summary-section="payment-lines">
                           <table class="total-line-table">
                              <caption class="visually-hidden">Cost summary</caption>
                              <thead>
                                 <tr>
                                    <th scope="col"><span class="visually-hidden">Description</span></th>
                                    <th scope="col"><span class="visually-hidden">Price</span></th>
                                 </tr>
                              </thead>
                              <tbody class="total-line-table__tbody">
                                 <tr class="total-line total-line--subtotal">
                                    <th class="total-line__name" scope="row">Subtotal</th>
                                    <td class="total-line__price">
                                       <span class="order-summary__emphasis" data-checkout-subtotal-price-target="$<?php echo $subtotal_price; ?>">$<?php echo $subtotal_price; ?></span>
                                    </td>
                                 </tr>
                                 <tr class="total-line total-line--shipping">
                                    <th class="total-line__name" scope="row">
                                       <span>Shipping</span>
                                    </th>
                                    <td class="total-line__price">
                                       <span class="order-summary__emphasis" data-checkout-total-shipping-target="495">$<?php echo $shipping_cost; ?></span>
                                    </td>
                                 </tr>
                                 <!--<tr class="total-line total-line--taxes hidden" data-checkout-taxes="">
                                    <th class="total-line__name" scope="row">Taxes</th>
                                    <td class="total-line__price">
                                       <span class="order-summary__emphasis" data-checkout-total-taxes-target="0">$<?php echo ($line_item['line_price'] + $shipping_cost);?></span>
                                    </td>
                                 </tr>-->
                              </tbody>
                              <tfoot class="total-line-table__footer">
                                 <tr class="total-line">
                                    <th class="total-line__name payment-due-label" scope="row">
                                       <span class="payment-due-label__total">Total</span>
                                    </th>
                                    <td class="total-line__price payment-due">
                                       <span class="payment-due__currency">USD</span>
                                       <span class="payment-due__price" data-checkout-payment-due-target="<?php echo ($subtotal_price + $shipping_cost);?>">$<?php echo ($subtotal_price + $shipping_cost);?></span>
                                    </td>
                                 </tr>
                              </tfoot>
                           </table>
                           <div class="visually-hidden" aria-live="polite" aria-atomic="true" role="status">
                              Updated total price:
                              <span data-checkout-payment-due-target="1950">$19.50</span>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div id="partial-icon-symbols" class="icon-symbols" data-tg-refresh="partial-icon-symbols" data-tg-refresh-always="true">
                     <svg xmlns="http://www.w3.org/2000/svg">
                        <symbol id="spinner-button">
                           <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                              <path d="M20 10c0 5.523-4.477 10-10 10S0 15.523 0 10 4.477 0 10 0v2c-4.418 0-8 3.582-8 8s3.582 8 8 8 8-3.582 8-8h2z"></path>
                           </svg>
                        </symbol>
                        <symbol id="chevron-right">
                           <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 10 10">
                              <path d="M2 1l1-1 4 4 1 1-1 1-4 4-1-1 4-4"></path>
                           </svg>
                        </symbol>
                        <symbol id="down-arrow">
                           <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 12">
                              <path d="M10.817 7.624l-4.375 4.2c-.245.235-.64.235-.884 0l-4.375-4.2c-.244-.234-.244-.614 0-.848.245-.235.64-.235.884 0L5.375 9.95V.6c0-.332.28-.6.625-.6s.625.268.625.6v9.35l3.308-3.174c.122-.117.282-.176.442-.176.16 0 .32.06.442.176.244.234.244.614 0 .848"></path>
                           </svg>
                        </symbol>
                        <symbol id="mobile-phone">
                           <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
                              <path d="M5 2.99C5 1.34 6.342 0 8.003 0h7.994C17.655 0 19 1.342 19 2.99v18.02c0 1.65-1.342 2.99-3.003 2.99H8.003C6.345 24 5 22.658 5 21.01V2.99zM7 5c0-.552.456-1 .995-1h8.01c.55 0 .995.445.995 1v14c0 .552-.456 1-.995 1h-8.01C7.445 20 7 19.555 7 19V5zm5 18c.552 0 1-.448 1-1s-.448-1-1-1-1 .448-1 1 .448 1 1 1z" fill-rule="evenodd"></path>
                           </svg>
                        </symbol>
                     </svg>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!--<iframe src="./checkout_files/dialog.html" scrolling="no" tabindex="-1" aria-hidden="true" style="position: fixed; top: 0px; left: 0px; z-index: 99999; height: 0px; width: 0px; border: 0px;"></iframe>-->
   </body>
</html>
