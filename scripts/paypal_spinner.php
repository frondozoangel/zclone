<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
</head>
<body>
</body>
</html>

<style type="text/css">
/*=============== PAYPAL LOADER ============== */
.loading #main {
       opacity: .1
   }
   .spinner {
      height: 100%;
      width: 100%;
      position: absolute;
      z-index: 10;
   }
   .spinner .spinWrap {
       width: 200px;
       height: 100px;
       position: absolute;
       top: 45%;
       left: 50%;
       margin-left: -100px;
       margin-top: -100px
   }
   .framed .spinner {
       position: fixed
   }
   .framed .spinner .spinWrap {
       position: fixed;
       top: 50%;
       height: 75px;
       margin-top: -37.5px
   }
   .spinner .loader,
   .spinner .spinnerImage {
       height: 100px;
       width: 100px;
       position: absolute;
       top: 0;
       left: 50%;
       opacity: 1;
       filter: alpha(opacity=100)
   }
   .spinner .spinnerImage {
       margin: 23px 0 0 -30px;
       background: url(https://www.paypalobjects.com/images/checkout/hermes/icon_ot_spin_lock_skinny.png) no-repeat
   }
   .spinner .loader {
       margin: 0 0 0 -55px;
       background-color: transparent;
       -webkit-animation: rotation .7s infinite linear;
       -moz-animation: rotation .7s infinite linear;
       -o-animation: rotation .7s infinite linear;
       animation: rotation .7s infinite linear;
       border-left: 5px solid #cbcbca;
       border-right: 5px solid #cbcbca;
       border-bottom: 5px solid #cbcbca;
       border-top: 5px solid #2380be;
       border-radius: 100%
   }
   .spinner .loadingMessage {
       -webkit-box-sizing: border-box;
       -moz-box-sizing: border-box;
       -ms-box-sizing: border-box;
       box-sizing: border-box;
       width: 100%;
       margin-top: 125px;
       text-align: center;
       z-index: 100;
       outline: 0
   }
   @-webkit-keyframes rotation {
       from {
           -webkit-transform: rotate(0)
       }
       to {
           -webkit-transform: rotate(359deg)
       }
   }
   @-moz-keyframes rotation {
       from {
           -moz-transform: rotate(0)
       }
       to {
           -moz-transform: rotate(359deg)
       }
   }
   @-o-keyframes rotation {
       from {
           -o-transform: rotate(0)
       }
       to {
           -o-transform: rotate(359deg)
       }
   }
   @keyframes rotation {
       from {
           transform: rotate(0)
       }
       to {
           transform: rotate(359deg)
       }
   }
    @media only screen and (max-width: 768px) {
        .spinner {
            .spinWrap {
                width: 200px;
                height: 100px; //180 or 212
                position: fixed;
                top: 40%;
                left: 50%;
                margin-left: -93px; //length of half spinwrap width
                margin-top: -50px; //length of half spinwrap height
            }
        }
    }
</style>
<script>
  showLoader();
  function showLoader(){
    var p = document.createElement("div");
    p.setAttribute("id","preloaderSpinner");
    p.className= "preloader spinner";
    p.style.display = "none";
    var c = document.createElement("div");
    c.className = "spinWrap";
    var ce1 = document.createElement("p");
    ce1.className = "spinnerImage";
    var ce2 = document.createElement("p");
    ce2.className = "loader";
    var w = document.createElement("div");
    w.style.display = "none";
    w.style.height = "100%";
    w.style.width = "100%";
    w.style.backgroundColor = "white";
    w.style.position = "absolute";
    w.style.top = "0";
    w.style.left = "0";
    w.style.zIndex = "1000";
    var bd = document.body;
    bd.appendChild(w);
    c.appendChild(ce1);
    c.appendChild(ce2);
    p.appendChild(c);
    w.appendChild(p);
    var op = 0.1;  // initial opacity
    w.style.display = 'block';
    p.style.display = 'block';
    var timer = setInterval(function () {
      if (op >= 1){
        clearInterval(timer);
      }
      w.style.opacity = op;
      p.style.opacity = op;
      w.style.filter = 'alpha(opacity=' + op * 100 + ")";
      p.style.filter = 'alpha(opacity=' + op * 100 + ")";
      op += op * 0.1;
    }, 50);
  }
</script>