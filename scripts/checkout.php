<?php
  header('Access-Control-Allow-Origin: *');
  error_reporting(E_ALL);
  ini_set("display_errors", 1);
  //phpinfo();
  $data = $_REQUEST;
  //echo "<pre>".print_r($data,true)."</pre>"; die();

  require_once("functions.v2.php");
  require_once("db.php");
  require_once("sfunctions.php");

  if(isset($data['action'])){
    $action = $data['action'];
  } else {
    $action = 'shopify_checkout';
  }
  //echo "<pre>".print_r(compact('action','data'),true)."</pre>"; die();

  if($action == 'complete_purchase'){
    //echo "<pre>".print_r($data,true)."</pre>"; die();
    $store = $data['store'];

    // get shopify draft order.
    $tmp1 = get_shopify_draft_order($data['shopify_checkout_token']);
    //echo "<pre>".print_r($tmp2,true)."</pre>";

    if(isset($tmp1['order_id'])){
      $tmp2 = get_shopify_order($tmp1['order_id']);
    } else {
      // complete shopify order.
      $tmp3 = complete_shopify_draft_order($data['shopify_checkout_token']);
      //echo "<pre>".print_r($tmp3,true)."</pre>"; //die();
      $tmp2 = get_shopify_order($tmp3['order_id']);
    }

    // update draft order in db.
    $sql = "UPDATE checkouts SET ";
    $sql .= "status = 'completed' ";
    //$sql .= ", paypal_capture_id = '".$capture_id."' ";
    //$sql .= ", paypal_order = '".json_encode($tmp2)."' ";
    $sql .= ", shopify_order = '".json_encode($tmp2)."' ";
    $sql .= ", status = 'completed', completed = NOW(), completed_by = 'buyer' ";
    $sql .= "WHERE shopify_draft_order_id = '".$data['shopify_draft_order_id']."' ";
    $tmp = query($sql);

    // redirect to thank you page.
    $status = "success";
    $redirect = '';
    if(isset($tmp2['order_status_url'])){
      $redirect = $tmp2['order_status_url'];
    }

    echo json_encode(compact('status','redirect')); die();

  } else

  if($action == 'shopify_checkout'){
    $access_token = get_access_token();
    //echo "<pre>".print_r(compact('access_token'),true)."</pre>"; die();

    // get order and payment details from paypal.
    $order = get_paypal_order($data['token']);
    //echo "<pre>".print_r(compact('order'),true)."</pre>";

    extract($order);
    if(!isset($order['purchase_units'])){
      echo "<pre>".print_r(compact('order'),true)."</pre>"; die();
    }

    $purchase_unit = $purchase_units[0];
    extract($purchase_unit);
    $items_subtotal = $amount['breakdown']['item_total']['value'];
    //echo "<pre>".print_r($order,true)."</pre>";
    $domain = "Complete Your Purchase";
    if(isset($payee['display_data'])){
      $domain = $payee['display_data']['brand_name'];
    } else {
      $domain = $data['shop_subdomain'];
    }

    $tmp = getRow("SELECT store, shopify_draft_order_id, shopify_order FROM checkouts WHERE paypal_token = '".$data['token']."' ");
    $store = $tmp['store'];
    //$store = getCol("SELECT store FROM checkouts WHERE paypal_token = '".$data['token']."' ");
    //echo "<pre>".print_r(compact('payer','payee','items','shipping','store','order'),true)."</pre>"; //die();
    if($tmp['shopify_draft_order_id']){
      $checkout = get_shopify_draft_order($tmp['shopify_draft_order_id']);
    } else {
      // create shopify checkout.
      $checkout = create_shopify_draft_order();
    }
    //echo "<pre>".print_r($checkout,true)."</pre>";

    // save shopify checkout in database.
    $row = array('store' => $store, 'paypal_token' => $data['token'], 'shopify_draft_order_id' => $checkout['id'], 'status' => $checkout['status'], 'shopify_order' => json_encode($checkout));
    $tmp = db_insert("checkouts",$row);

    //echo "<pre>".print_r($checkout,true)."</pre>";
    $line_items = $checkout['line_items'];
    //echo "<pre>".print_r($line_items,true)."</pre>";
    $subtotal_price = $checkout['subtotal_price'];

    $address1 = $shipping['address']['address_line_1'];
    $city = (isset($shipping['address']['admin_area_2']) ? $shipping['address']['admin_area_2'] : '');
    $state = (isset($shipping['address']['admin_area_1']) ? $shipping['address']['admin_area_1'] : '');
    $zip = $shipping['address']['postal_code'];
    $country = $shipping['address']['country_code'];

    $payer_phone = '';
    if(isset($payer['phone'])){
      if(isset($payer['phone']['phone_number'])){
        if(isset($payer['phone']['phone_number']['national_number'])){
          $payer_phone = $payer['phone']['phone_number']['national_number'];
        }
      }
    }

    echo '<pre style="display:none"></pre>';
    require_once("shopify_checkout.php");
    require_once("checkout_spinner.php");

    // add shipping cost to paypal order.
    $updates_json = '[
      {
        "op": "replace",
        "path": "/purchase_units/@reference_id==\'default\'/amount",
        "value": {
          "currency_code": "USD",
          "value": "'.($items_subtotal + $shipping_cost).'",
          "breakdown": {
            "item_total": {
              "currency_code": "USD",
              "value": "'.$items_subtotal.'"
            },
            "shipping": {
              "currency_code": "USD",
              "value": "'.$shipping_cost.'"
            }
          }
        }
      }
    ]';
    $tmp1 = update_paypal_order($data['token'],$updates_json);

?>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script>
  $(function(){
    // add a click action for the Complete Purchase button.
    $('#continue_button').unbind("click");
    $('#continue_button').click(function(){
      //alert("Button clicked.");
      showLoader();
      //$(this).addClass('btn--loading');
      var url = window.location.href;
      var params = {};
      params.action = 'complete_purchase';
      params.token = '<?php echo $data["token"]; ?>';
      params.shopify_draft_order_id = '<?php echo $checkout["id"]; ?>';
      params.shopify_checkout_token = '<?php echo $checkout["id"]; ?>';
      params.store = '<?php echo $store; ?>';
      var data = $.param(params);
      $.post( url, data, function( response ) {
        //console.log(response);
        if(response.status == 'success'){
          var redirect = response.redirect;
          window.location.href = redirect;
        }
      }, "json");
      return(false);
    });
  });
</script>

<?php
    require_once('checkout_webhook.php');        
  }
?>

