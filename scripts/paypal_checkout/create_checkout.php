<?php
	session_start();

	require __DIR__.'/vendor/autoload.php';
	use phpish\shopify;

	require __DIR__.'/conf.php';

	$shopify = shopify\client($_SESSION['shop'], SHOPIFY_APP_API_KEY, $_SESSION['oauth_token']);

	try
	{
		# Making an API request can throw an exception
        $json = '{
          "checkout":{
            "email": "john.smith@example.com",
            "line_items": [{
              "variant_id": 29778623070307,
              "quantity": 2
            }],
            "shipping_address": {
              "first_name": "John",
              "last_name": "Smith",
              "address1": "126 York St.",
              "city": "Ottawa",
              "province_code": "ON",
              "country_code": "CA",
              "phone": "(123)456-7890",
              "zip": "K1N 5T5"
            }
          }
        }';
        $payload = json_decode($json,true);
        echo "<pre>".print_r(compact('payload'),true)."</pre>";
		$tmp = $shopify('POST /admin/checkouts.json', array(),$json);
		echo "<pre>".print_r($tmp,true)."</pre>";
	}
	catch (shopify\ApiException $e)
	{
		# HTTP status code was >= 400 or response contained the key 'errors'
		echo $e;
		echo "<pre>".print_r($e->getRequest(),true)."</pre>";
		echo "<pre>".print_r($e->getResponse(),true)."</pre>";
	}
	catch (shopify\CurlException $e)
	{
		# cURL error
		echo $e;
		echo "<pre>".print_r($e->getRequest(),true)."</pre>";
		echo "<pre>".print_r($e->getResponse(),true)."</pre>";
	}
