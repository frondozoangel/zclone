<?php

	session_start();
    require ('../logs.php');
    $filename = basename(__FILE__);
    start_log();
    $request = $_REQUEST;
    $session = $_SESSION;
    $headers = getallheaders();
    $json = file_get_contents('php://input');
    $data = json_decode($json);
    echo "<pre>".print_r(compact('headers','data','request','session'),true)."</pre>";
    end_log();

	require __DIR__.'/vendor/autoload.php';
	use phpish\shopify;

	require __DIR__.'/conf.php';
    error_reporting(E_ALL);
    ini_set("display_errors", 1);

	# Guard: http://docs.shopify.com/api/authentication/oauth#verification
	shopify\is_valid_request($_GET, SHOPIFY_APP_SHARED_SECRET) or die('Invalid Request! Request or redirect did not come from Shopify');

	# Step 2: http://docs.shopify.com/api/authentication/oauth#asking-for-permission
	if (!isset($_GET['code']))
	{
        //echo "<pre>".print_r($_SERVER,true)."</pre>"; die();
        $protocol = ((!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
        $redirect_uri = $protocol . $_SERVER['HTTP_HOST'] . $_SERVER['SCRIPT_NAME'];
        //echo "<pre>".print_r(compact('redirect_uri'),true)."</pre>"; die();
		$permission_url = shopify\authorization_url($_GET['shop'], SHOPIFY_APP_API_KEY, array('read_content', 'write_content', 'read_themes', 'write_themes', 'read_products', 'write_products', 'read_customers', 'write_customers', 'read_draft_orders', 'write_draft_orders', 'read_orders', 'write_orders', 'read_script_tags', 'write_script_tags', 'read_fulfillments', 'write_fulfillments', 'read_shipping', 'write_shipping', 'read_checkouts', 'write_checkouts'),$redirect_uri);
		die("<script> top.location.href='$permission_url'</script>");
	}


	# Step 3: http://docs.shopify.com/api/authentication/oauth#confirming-installation
	try
	{
		# shopify\access_token can throw an exception
		$oauth_token = shopify\access_token($_GET['shop'], SHOPIFY_APP_API_KEY, SHOPIFY_APP_SHARED_SECRET, $_GET['code']);

		$_SESSION['oauth_token'] = $oauth_token;
		$shop = $_SESSION['shop'] = $_GET['shop'];

		//echo 'App Successfully Installed!';
        require("../db.php");
        $tmp = db_insert("tokens",compact('shop','oauth_token'));

        $shop_apps_url = "https://".$_GET['shop']."/admin/apps";
        die("<script> top.location.href='$shop_apps_url'</script>");
	}
	catch (shopify\ApiException $e)
	{
		# HTTP status code was >= 400 or response contained the key 'errors'
		echo $e;
		print_R($e->getRequest());
		print_R($e->getResponse());
	}
	catch (shopify\CurlException $e)
	{
		# cURL error
		echo $e;
		print_R($e->getRequest());
		print_R($e->getResponse());
	}


?>