<?php
  session_start();
  header('Access-Control-Allow-Origin: *');
  error_reporting(E_ALL);
  ini_set("display_errors", 1);

  //phpinfo();
  require ('logs.php');
  $filename = basename(__FILE__);
  start_log();
  $request = $_REQUEST;
  $session = $_SESSION;
  $headers = getallheaders();
  $json = file_get_contents('php://input');
  $data = json_decode($json);
  //echo "<pre>".print_r(compact('headers','data','request','session'),true)."</pre>";
  end_log();
  $data = $request;
  //echo "<pre>".print_r($_REQUEST,true)."</pre>"; die();

  require("functions.v2.php");
  $tmp = create_paypal_order();
  $links = array();
  foreach($tmp['links'] as $link){
    $links[$link['rel']] = $link['href'];
  }
  //echo "<pre>".print_r($links,true)."</pre>"; die();

  require_once("db.php");
  $row = array('domain' => $data['shop_domain'], 'store' => $data['shop_subdomain'], 'paypal_token' => $tmp['id'], 'paypal_order' => json_encode($tmp), 'cookies' => $data['cookies']);
  $tmp2 = db_insert("checkouts",$row);

  if(isset($data['redirect'])){
    require("paypal_spinner.php");
    if(isset($links['approve'])){
      echo '<script>window.location.href = "'.$links['approve'].'";</script>';
    } else {
      echo "<pre>".print_r($tmp,true)."</pre>";
    }
  } else {
    echo json_encode(compact('links')); die();
  }

?>

