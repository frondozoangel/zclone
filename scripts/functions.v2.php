<?php

  require_once("rest.php");
  require_once("settings.php");

  function get_access_token(){
    global $data,$access_token_url,$payment_url,$client_id,$client_secret;

    /* get an access token.
    // ref: https://developer.paypal.com/docs/api/get-an-access-token-curl/
    curl -v https://api.sandbox.paypal.com/v1/oauth2/token \
       -H "Accept: application/json" \
       -H "Accept-Language: en_US" \
       -u "client_id:secret" \
       -d "grant_type=client_credentials"
    */
    $params = array('grant_type' => 'client_credentials');
    //echo "<pre>".print_r(compact('access_token_url','params','client_id','client_secret'),true)."</pre>";

    $ret = '';
    //echo "<pre>".print_r(compact('client_id','client_secret'),true)."</pre>"; die();
    if(trim($client_id) == '' || trim($client_secret) == ''){
      $ret = array('status' => 'error', 'message' => 'invalid client id or client secret.');
      echo json_encode($ret); die();
    }
    $response = httpPost($access_token_url,$params,$client_id,$client_secret);
    //echo $response;
    if($response){
      $response = json_decode($response,true);
      if(isset($response['access_token'])){
        $ret = $response['access_token'];
      } else {
        //echo json_encode($response); die();
        $ret = array('status' => 'error', 'message' => $response['error_description']);
        echo json_encode($ret); die();
      }
    }

    if($ret == ''){
      $ret = array('status' => 'error', 'message' => 'failed to get access token');
      echo json_encode($ret); die();
    }
    return($ret);
  }

  function create_paypal_order(){

    global $data,$payment_url,$client_id,$client_secret;

    /* map the data to the payments api structure.
    // ref: https://developer.paypal.com/docs/integration/direct/payments/authorize-and-capture-payments/#authorize-the-payment
    */

    $access_token = get_access_token();
    //echo "<pre>".print_r(compact('access_token','data'),true)."</pre>";

    $items_json = '';
    foreach($data['items'] as $item){
      if($items_json){
        $items_json .= ',
        ';
      }

      $description = strip_tags($item['product_description']);
      $description = str_replace(array("\r", "\n"), ' ',$description);
      $description = trim($description);
      if(strlen($description) > 50){
        $description = substr($description,0,48)."..";
      }
      $items_json .= '{
        "name": "'.$item['product_title'].'",
        "description": "'.$description.'",
        "quantity": "'.$item['quantity'].'",
        "unit_amount": {
          "currency_code": "USD",
          "value": "'.($item['price']/100).'"
        },
        "sku": "'.$item['variant_id'].'",
        "currency": "USD"
      }';
    }

    $brand = $data['cancel_url'];
    $brand = str_replace("https://","",$brand);
    $brand = str_replace("/cart","",$brand);
    $json = '{
      "intent": "CAPTURE",
      "purchase_units": [
        {
          "amount": {
            "currency_code": "USD",
            "value": "'.($data['items_subtotal_price']/100).'",
            "breakdown": {
              "item_total": {
                "currency_code": "USD",
                "value": "'.($data['items_subtotal_price']/100).'"
              }
            }
          },
          "items": [
          '.$items_json.'
          ]
        }
      ],
      "application_context": {
        "brand_name": "'.$brand.'",
        "landing_page": "BILLING",
        "return_url": "'.$data['return_url']."?shop_subdomain=".$data['shop_subdomain'].'",
        "cancel_url": "'.$data['cancel_url'].'"
      }
    }';

    //echo "<pre>".print_r(compact('json'),true)."</pre>";
    $data2 = json_decode($json,true);
    $data_string = json_encode($data2);
    $headers =  array(
        'Content-Type: application/json',
        'Content-Length: ' . strlen($data_string),
        'Authorization: Bearer '.$access_token
    );

    //echo "<pre>".print_r(compact('payment_url','data_string','headers'),true)."</pre>"; //die();
    $result = httpPost($payment_url,$data2,null,null,true,$headers);
    //echo $result;
    if($result){
      $result = json_decode($result,true);
    }

    //echo "<pre>".print_r(compact('result'),true)."</pre>"; //die();
    return($result);
  }

  /* get order details.
  // ref: https://developer.paypal.com/docs/api/orders/v2/#orders_get
  curl -v -X GET https://api.sandbox.paypal.com/v2/checkout/orders/5O190127TN364715T \
  -H "Content-Type: application/json" \
  -H "Authorization: Bearer Access-Token"
  */
  function get_paypal_order($order_id){
    global $data,$access_token,$get_order_url,$client_id,$client_secret;

    $headers =  array(
        'Content-Type: application/json',
        'Authorization: Bearer '.$access_token
    );

    $ret = '';
    $url = $get_order_url;
    $url = str_replace("{order_id}",$order_id,$url);

    //echo "<pre>".print_r(compact('url','headers'),true)."</pre>"; //die();
    $response = httpGet($url,null,null,$headers);
    //echo $response; die();
    if($response){
      $response = json_decode($response,true);
    }

    return($response);
  }

  function update_paypal_order($order_id,$json){

    global $data,$access_token,$payment_url,$client_id,$client_secret;

    $data2 = json_decode($json,true);
    //echo "<pre>".print_r($data2,true)."</pre>";
    $data_string = json_encode($data2);
    $headers =  array(
        'Content-Type: application/json',
        'Content-Length: ' . strlen($data_string),
        'Authorization: Bearer '.$access_token
    );

    //echo "<pre>".print_r(compact('payment_url','data_string','headers'),true)."</pre>"; //die();
    $result = httpPatch($payment_url."/".$order_id,$data2,null,null,true,$headers);
    //echo $result;
    if($result){
      $result = json_decode($result,true);
      //echo "<pre>".print_r(compact('result'),true)."</pre>"; //die();
    }

    return($result);
  }

  function update_paypal_order2(){

    global $data,$payment_url,$client_id,$client_secret;

    $items_json = '';
    foreach($data['line_items'] as $item){
      if($items_json){
        $items_json .= ',
        ';
      }
      $description = strip_tags(isset($item['product_description']) ? $item['product_description'] : "");
      $description = str_replace(array("\r", "\n"), ' ',$description);
      $description = trim($description);
      if(strlen($description) > 50){
        $description = substr($description,0,48)."..";
      }
      $items_json .= '{
        "name": "'.$item['title'].'",
        "description": "'.$description.'",
        "quantity": "'.$item['quantity'].'",
        "unit_amount": {
          "currency_code": "USD",
          "value": "'.($item['price']).'"
        },
        "sku": "'.$item['variant_id'].'",
        "currency": "USD"
      }';
    }

    $items_subtotal = $data['subtotal_price'];
    $shipping_price = $data['shipping_line']['price'];
    $purchase_units_json = '
        {
          "amount": {
            "currency_code": "USD",
            "value": "'.($items_subtotal + $shipping_price).'",
            "breakdown": {
              "item_total": {
                "currency_code": "USD",
                "value": "'.$items_subtotal.'"
              },
              "shipping": {
                "currency_code": "USD",
                "value": "'.$shipping_price.'"
              }
            }
          },
          "items": [
          '.$items_json.'
          ]
        }
      ';

    $updates_json = '[
      {
        "op": "replace",
        "path": "/purchase_units/@reference_id==\'default\'",
        "value": '.$purchase_units_json.'
      }
    ]';

    $updates_data = json_decode($updates_json,true);
    //echo "<pre>".print_r(compact('data','updates_json','updates_data'),true)."</pre>";
    $tmp1 = update_paypal_order($data['token'],$updates_json);
    //echo "<pre>".print_r(compact('tmp1'),true)."</pre>";
    return($tmp1);
  }

  function create_paypal_order2(){

    global $data,$payment_url,$client_id,$client_secret,$access_token;

    $items_json = '';
    foreach($data['line_items'] as $item){
      if($items_json){
        $items_json .= ',
        ';
      }
      $description = strip_tags(isset($item['product_description']) ? $item['product_description'] : "");
      $description = str_replace(array("\r", "\n"), ' ',$description);
      $description = trim($description);
      if(strlen($description) > 50){
        $description = substr($description,0,48)."..";
      }
      $items_json .= '{
        "name": "'.$item['title'].'",
        "description": "'.$description.'",
        "quantity": "'.$item['quantity'].'",
        "unit_amount": {
          "currency_code": "USD",
          "value": "'.($item['price']).'"
        },
        "sku": "'.$item['variant_id'].'",
        "currency": "USD"
      }';
    }

    $items_subtotal = $data['subtotal_price'];
    $shipping_price = $data['shipping_line']['price'];
    $purchase_units_json = '[
        {
          "amount": {
            "currency_code": "USD",
            "value": "'.($items_subtotal + $shipping_price).'",
            "breakdown": {
              "item_total": {
                "currency_code": "USD",
                "value": "'.$items_subtotal.'"
              },
              "shipping": {
                "currency_code": "USD",
                "value": "'.$shipping_price.'"
              }
            }
          },
          "items": [
          '.$items_json.'
          ]
        }
      ]';


    $brand = $data['domain'];
    $json = '{
      "intent": "CAPTURE",
      "purchase_units": '.$purchase_units_json.',
      "application_context": {
        "brand_name": "'.$brand.'",
        "landing_page": "BILLING",
        "return_url": "'.$data['return_url'].'",
        "cancel_url": "'.$data['cancel_url'].'"
      }
    }';

    //echo "<pre>".print_r(compact('json'),true)."</pre>";
    $data2 = json_decode($json,true);
    $data_string = json_encode($data2);
    $headers =  array(
        'Content-Type: application/json',
        'Content-Length: ' . strlen($data_string),
        'Authorization: Bearer '.$access_token
    );

    //echo "<pre>".print_r(compact('payment_url','data_string','headers'),true)."</pre>"; //die();
    $result = httpPost($payment_url,$data2,null,null,true,$headers);
    //echo $result;
    if($result){
      $result = json_decode($result,true);
    }

    //echo "<pre>".print_r(compact('result'),true)."</pre>"; //die();
    return($result);
  }

  function capture_paypal_payment($order_id){
    global $data,$access_token,$get_order_url,$client_id,$client_secret;

    $headers =  array(
        'Content-Type: application/json',
        'Authorization: Bearer '.$access_token,
    );

    $order = get_paypal_order($order_id);
    //echo "<pre>".print_r(compact('order'),true)."</pre>"; //die();

    $rows = $order['links'];
    $links = array();
    foreach($rows as $row){
      $links[$row['rel']] = $row['href'];
    }

    $response = $order;
    if(isset($links['capture'])){
      $url = $links['capture'];
      //echo "<pre>".print_r(compact('url','headers'),true)."</pre>"; //die();
      $response = httpPost($url,array(),null,null,true,$headers);
      //echo $response; die();
      if($response){
        $response = json_decode($response,true);
        //echo "<pre>".print_r(compact('response'),true)."</pre>"; //die();
      }
    } 

    return($response);
  }


?>