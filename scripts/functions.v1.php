<?php

  require_once("settings.php");
  require_once("rest.php");

  function get_access_token(){
    global $data,$access_token_url,$payment_url,$client_id,$client_secret;

    /* get an access token.
    // ref: https://developer.paypal.com/docs/api/get-an-access-token-curl/
    curl -v https://api.sandbox.paypal.com/v1/oauth2/token \
       -H "Accept: application/json" \
       -H "Accept-Language: en_US" \
       -u "client_id:secret" \
       -d "grant_type=client_credentials"
    */
    $params = array('grant_type' => 'client_credentials');
    //echo "<pre>".print_r(compact('access_token_url','params','client_id','client_secret'),true)."</pre>";

    $ret = '';
    $response = httpPost($access_token_url,$params,$client_id,$client_secret);
    //echo $response;
    if($response){
      $response = json_decode($response,true);
      $ret = $response['access_token'];
    }
    return($ret);
  }

  function create_payment(){

    global $data,$payment_url,$client_id,$client_secret;

    /* map the data to the payments api structure.
    // ref: https://developer.paypal.com/docs/integration/direct/payments/authorize-and-capture-payments/#authorize-the-payment
    */

    $access_token = get_access_token();
    //echo "<pre>".print_r(compact('access_token','data'),true)."</pre>";

    $items_json = '';
    foreach($data['items'] as $item){
      if($items_json){
        $items_json .= ',
        ';
      }

      $description = strip_tags($item['product_description']);
      $description = str_replace(array("\r", "\n"), ' ',$description);
      $description = trim($description);
      if(strlen($description) > 50){
        $description = substr($description,0,48)."..";
      }
      $items_json .= '{
        "name": "'.$item['product_title'].'",
        "description": "'.$description.'",
        "quantity": "'.$item['quantity'].'",
        "price": "'.$item['price'].'",
        "tax": "0.00",
        "sku": "'.$item['sku'].'",
        "currency": "USD"
      }';
    }

    $json = '{
      "intent": "authorize",
      "payer":
      {
        "payment_method": "paypal"
      },
      "transactions": [
      {
        "amount":
        {
          "total": "'.(floatval($data['items_subtotal_price']) + 4.95).'",
          "currency": "USD",
          "details":
          {
            "subtotal": "'.$data['items_subtotal_price'].'",
            "tax": "0.00",
            "shipping": "4.95",
            "handling_fee": "0.00",
            "shipping_discount": "0.00",
            "insurance": "0.00"
          }
        },
        "description": "This is the payment transaction description.",
        "custom": "Shopify_'.$data['token'].'",
        "invoice_number": "'.$data['token'].'",
        "payment_options":
        {
          "allowed_payment_method": "INSTANT_FUNDING_SOURCE"
        },
        "soft_descriptor": "'.$data['token'].'",
        "item_list":
        {
          "items": [
          '.$items_json.'
          ]
        },
        "related_resources": []
      }],
      "note_to_payer": "Contact us for any questions on your order.",
      "redirect_urls":
      {
        "return_url": "https://example.com/return",
        "cancel_url": "https://example.com/cancel"
      }
    }';

    //echo "<pre>".print_r(compact('json'),true)."</pre>";
    $data2 = json_decode($json,true);
    $data_string = json_encode($data2);
    $headers =  array(
        'Content-Type: application/json',
        'Content-Length: ' . strlen($data_string),
        'Authorization: Bearer '.$access_token
    );

    //echo "<pre>".print_r(compact('payment_url','data_string','headers'),true)."</pre>"; //die();
    $result = httpPost($payment_url,$data2,null,null,true,$headers);
    //echo $result;
    if($result){
      $result = json_decode($result,true);
    }

    //echo "<pre>".print_r(compact('result'),true)."</pre>"; //die();
    return($result);
  }

?>