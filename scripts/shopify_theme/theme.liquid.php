<!doctype html>
<html class="no-js" lang="{{ shop.locale }}">
<head>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-KDDL4C3');</script>
<!-- End Google Tag Manager -->
  <!-- Basic page needs ================================================== -->
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

  {% if settings.favicon %}
    <link rel="shortcut icon" href="{{ settings.favicon | img_url: '32x32' }}" type="image/png" />
  {% endif %}

  <!-- Title and description ================================================== -->
  <title>
  {{ page_title }}{% if current_tags %}{% assign meta_tags = current_tags | join: ', ' %} &ndash; {{ 'general.meta.tags' | t: tags: meta_tags }}{% endif %}{% if current_page != 1 %} &ndash; {{ 'general.meta.page' | t: page: current_page }}{% endif %}{% unless page_title contains shop.name %} &ndash; {{ shop.name }}{% endunless %}
  </title>

  {% if page_description %}
  <meta name="description" content="{{ page_description | escape }}">
  {% endif %}

  <!-- Helpers ================================================== -->
  {% include 'social-meta-tags' %}
  <link rel="canonical" href="{{ canonical_url }}">
  <meta name="viewport" content="width=device-width,initial-scale=1,shrink-to-fit=no">
  <meta name="theme-color" content="{{ settings.color_primary }}">

  <!-- CSS ================================================== -->
  {{ 'timber.scss.css' | asset_url | stylesheet_tag }}
  {{ 'theme.scss.css' | asset_url | stylesheet_tag }}

  <!-- Sections ================================================== -->
  <script>
    window.theme = window.theme || {};
    theme.strings = {
      zoomClose: {{ "products.zoom.close" | t | json }},
      zoomPrev: {{ "products.zoom.prev" | t | json }},
      zoomNext: {{ "products.zoom.next" | t | json }},
      moneyFormat: {{ shop.money_format | json }},
      addressError: {{ 'home_page.map.address_error' | t | json }},
      addressNoResults: {{ 'home_page.map.address_no_results' | t | json }},
      addressQueryLimit: {{ 'home_page.map.address_query_limit_html' | t | json }},
      authError: {{ 'home_page.map.auth_error_html' | t | json }},
      cartEmpty: {{ 'cart.general.empty' | t | json }},
      cartCookie: {{ 'cart.general.cookies_required' | t | json }},
      cartSavings: {{ 'cart.general.savings_html' | t: savings: '[savings]' | json }}
    };
    theme.settings = {
      cartType: {{ settings.cart_type | json }},
      gridType: {{ settings.collection_products_grid | json }}
    };
  </script>

  {{ 'jquery-2.2.3.min.js' | asset_url | script_tag }}

  <script src="{{ 'lazysizes.min.js' | asset_url }}" async="async"></script>

  <script src="{{ 'theme.js' | asset_url }}" defer="defer"></script>

  <!-- Header hook for plugins ================================================== -->
  {{ content_for_header }}

  {{ 'modernizr.min.js' | asset_url | script_tag }}

  {% comment %}
    If you store has customer accounts disabled, you can remove the following JS file
  {% endcomment %}
  {% if template.directory == 'customers' %}
    {{ 'shopify_common.js' | shopify_asset_url | script_tag }}
  {% endif %}

</head>

<script type="text/javascript">
window.addEventListener('load', function(){

    function getPageType() {
    {% if template.name == "index" %}
        return "Homepage";
    {% elsif search.performed %}
        return "Search Results";
    {% elsif template.name == "collection" %}
        return "Collection Page";
    {% elsif template.name == "product" %}
        return "Product Detail Page";
    {% elsif template.name == "checkout" %}
        return "Checkout Page";
    {% else %}
        return 'Other';
    {% endif %}
}

{% if customer %}
    window.dataLayer.push({
        visitorType: "Logged In",
        VisitorId: "{{ customer.id }}",
        CustomerId: "{{ customer.id }}",
        CustomerEmail: "{{ customer.email }}",
        CustomerFirstName: "{{ customer.first_name }}",
        CustomerLastName: "{{ customer.last_name }}",
        CustomerOrdersCount: {{ customer.orders_count }},
        CustomerTotalSpent: {{ customer.total_spent | divided_by: 100 }},
    });
{% else %}
    window.dataLayer.push({
        visitorType: "Guest"
    });
{% endif %}

{% if template.name == "index" %}
    window.dataLayer.push({
        event: "HomeView",
        pageType: "home"
    });
{% elsif search.performed %}
    window.dataLayer.push({
        pageType: "searchresults"
    });
{% elsif template.name == "collection" %}
    window.dataLayer.push({
        pageType: "category"
    });
{% elsif template.name == "product" %}
    window.dataLayer.push({
        pageType: "product"
    });
{% elsif template.name == "cart" %}
    window.dataLayer.push({
        pageType: "cart"
    });
{% elsif template.name == "checkout" %}
    window.dataLayer.push({
        pageType: "checkout"
    });
{% else %}
    window.dataLayer.push({
        pageType: "other"
    });
{% endif %}


    {% if template.name == "product" %}
        var product = {{ product | json }};

        function getCurrentVariant() {
            var selects = document.querySelectorAll('form[action^="/cart/add"] select[name="id"]');
            if (!selects.length) return product.variants[0];
            var selectedId = selects[0].value;
            return product.variants.filter(function(variant) {
                return variant.id == selectedId;
            })[0];
        }

        var variant = getCurrentVariant();

        if (variant) {
            window.dataLayer.push({
                VariantCompareAtPrice: variant.compare_at_price / 100,
                VariantPrice: variant.price / 100,
                VariantInventoryQuantity: variant.inventory_quantity,
                shopifyProductId: "shopify_US_{{product.id}}_" + variant.id
            });
        }

        window.dataLayer.push({
            event: 'productDetailView',
            ecommerce: {
                currencyCode: "{{ shop.currency }}",
                detail: {
                    products: [{
                        name: product.title,
                        id: product.id,
                        price: product.price / 100,
                        brand: product.vendor,
                        variant: variant && variant.title,
                        {% if collection %}
                            category: "{{ collection.title }}",
                        {% endif %}
                    }]
                }
            }
        });

        var handleVariantChange = function(event) {
            var variant = getCurrentVariant();

            window.dataLayer.push({
                event: 'productDetailView',
                ecommerce: {
                    currencyCode: "{{ shop.currency }}",
                    detail: {
                        products: [{
                            name: product.title,
                            id: product.id,
                            price: product.price / 100,
                            brand: product.vendor,
                            variant: variant.title,
                            {% if collection %}
                                category: "{{ collection.title }}",
                            {% endif %}
                        }]
                    }
                }
            });

            window.dataLayer.push({
                VariantCompareAtPrice: variant.compare_at_price / 100,
                VariantPrice: variant.price / 100,
                VariantInventoryQuantity: variant.inventory_quantity,
                shopifyProductId: "shopify_{{shop.currency}}_{{product.id}}_" + variant.id
            });
        };

        Array.prototype.slice.call(document.querySelectorAll('form[action^="/cart/add"] .single-option-selector'))
            .forEach(function(select) {
                select.addEventListener('change', handleVariantChange);
            });

        document.querySelectorAll("form[action^='/cart/add']")[0].addEventListener("submit", function() {
            var variant = getCurrentVariant();

            window.dataLayer.push({
                event: "addToCart",
                ecommerce: {
                    currencyCode: ShopifyAnalytics.meta.currency,
                    add: {
                        products: [
                            {
                                name: product.title,
                                id: product.id,
                                price: variant.price / 100,
                                brand: product.vendor,
                                variant: variant.title,
                                quantity: document.getElementById("Quantity") ? Number(document.getElementById("Quantity").value) : 1,
                                {% if collection %}
                                    category: "{{ collection.title }}",
                                {% endif %}
                            }
                        ]
                    }
                }
            });
        });
    {% endif %}

    {% if template.name == "cart" %}
        var cart = {{ cart | json }};

        var cartItems = cart.items.map(function(item, idx) {
            return {
                position: idx,
                id: item.product_id,
                name: item.product_title,
                quantity: item.quantity,
                price: item.price / 100,
                brand: item.vendor,
                variant: item.variant_title
            };
        });

        if (cart.items.length) {
            var cartItemSkus = cart.items.length === 1
                ? cart.items[0].product_id
                : cart.items.map(function(item, idx) {
                    return item.product_id;
                });

            var shopifyCartItemSkus = cart.items.length === 1
                ? "shopify_US_" + cart.items[0].id + "_" + cart.items[0].variant_id
                : cart.items.map(function(item, idx) {
                    return "shopify_US_" + item.product_id + "_" + item.variant_id;
                });

            window.dataLayer.push({
                cartItems: cartItemSkus,
                shopifyCartItemSkus: shopifyCartItemSkus
            });
        }

        /*
        window.dataLayer.push({
            event: "cartView",
            ecommerce: {
                currencyCode: "{{ shop.currency }}",
                detail: {
                    actionField: { list: "Shopping Cart" },
                    products: cartItems,
                    totalPrice: cart.total_price / 100,
                    itemNumber: cart.item_count
                }
            }
        });
        */

        window.dataLayer.push({
            cartTotal: "{{ cart.total_price | money_without_currency | remove:',' }}",
            ecommerce: {
                currencyCode: "{{ shop.currency }}",
                actionField: { list: "Shopping Cart" },
                impressions: cartItems,
            }
        });

        var handleRemoveClick = function(event) {
            var index = Number(event.target.href.split('line=')[1].split('&')[0]) - 1;

            item = cartItems.filter(function(item) {
                return item.position === index;
            });

            window.dataLayer.push({
                event: "removeFromCart",
                ecommerce: {
                    remove: {
                        products: item,
                    }
                }
            });
        };

        // "Remove" button tracking
        Array.prototype.slice.call(document.querySelectorAll('a[href*="quantity=0"]'))
            .forEach(function(button) {
                button.addEventListener('click', handleRemoveClick);
            });
    {% endif %}

      {% if search.performed %}
        var query = "{{ search.terms }}";

        window.dataLayer.push({
            event: "SearchView",
            SearchTerms: query
        });

        var collection = [];

        {% for item in search.results %}
            {% if item.object_type == 'product' %}
                collection.push({{ item | json }});
            {% endif %}
            console.log('{{ item.object_type }}');
        {% endfor %}

        if (!collection.length) collection= [];

        var allProducts = collection.map(function(product, index) {
                return {
                    name: product.title,
                    id: product.variants[0].sku,
                    price: product.price / 100,
                    brand: product.vendor,
                    position: index,
                    list: "Search Results",
                    handle: product.handle
                };
            });

        var visibleProducts = [];
        Array.prototype.slice.call(document.querySelectorAll('a[href*="/products/"]')).forEach(function(item) {
            var arr = item.href.split('/products/');
            var handle = arr[arr.length-1];

            if (!allProducts.length) return;

            var matchingProduct = allProducts.filter(function(product) {
                return product.handle === handle;
            });

            if (!matchingProduct.length) return;

            if (visibleProducts.indexOf(matchingProduct[0]) == -1) {
                visibleProducts.push(matchingProduct[0]);
            }
        })

        window.dataLayer.push({
            event: 'searchResults',
            ecommerce: {
                currencyCode: "{{ shop.currency }}",
                actionField: { list: "Search Results" },
                impressions: visibleProducts,
            }
        });
    {% endif %}

    {% if template.name == "collection" %}
        var collection = {{ collection | json }};
        if (!collection) collection = 0;
        var collectionTitle = "{{ collection.title }}";
        if (!collection) collectionTitle = 'All Products';

        var allProducts = {{ collection.products | json }}.map(function(product, index) {
            return {
                name: product.title,
                id: product.id,
                price: product.price / 100,
                brand: product.vendor,
                position: index,
                category: collectionTitle,
                list: getPageType(),
                handle: product.handle
            };
        });

        var visibleProducts = [];

        Array.prototype.slice.call(document.querySelectorAll('a[href*="/products/"]')).forEach(function(item) {
            var arr = item.href.split('/products/');
            var handle = arr[arr.length-1];

            if (!allProducts.length) return;

            var matchingProduct = allProducts.filter(function(product) {
                return product.handle === handle;
            });

            if (!matchingProduct.length) return;

            if (visibleProducts.indexOf(matchingProduct[0]) == -1) {
                visibleProducts.push(matchingProduct[0]);
            }
        });


        window.dataLayer.push({
            event: 'collectionView',
            ecommerce: {
                currencyCode: "{{ shop.currency }}",
                actionField: { list: "Collection Page" },
                impressions: visibleProducts,
            }
        });

    {% endif %}


    Array.prototype.slice.call(document.querySelectorAll('a[href*="/products/"]'))
        .forEach(function(select) {
            select.addEventListener('click', handleProductClick);
        });

    function getClickedProductHandle(element) {
        var arr = element.href.split('/products/');
        return arr[arr.length-1];
    }

    function handleProductClick(event) {
        if(typeof allProducts == "undefined") return;
        var target = event.target.matches('a[href*="/products/"]')
            ? event.target
            : event.target.closest('a[href*="/products/"]');
        var handle = getClickedProductHandle(target);
        var clickedProduct = allProducts.filter(function(product) {
            return product.handle === handle;
        });
        if (clickedProduct[0]) delete clickedProduct[0].list;
        if (clickedProduct[0]) delete clickedProduct[0].handle;
        window.dataLayer.push({
            'event': 'productClick',
            'ecommerce': {
                currencyCode: "{{ shop.currency }}",
                'click': {
                    'actionField': {'list': getPageType()},
                    'products': clickedProduct

                }
            },
        });
    }
}, false);
</script>
{% comment %}
  Add the page template as a class for easy page or template specific styling.
{% endcomment %}
<body id="{{ page_title | handle }}" class="{% if customer %}customer-logged-in {% endif %}template-{{ template | replace: '.', ' ' | truncatewords: 1, '' | handle }}">

  {% section 'header' %}

  <div id="CartDrawer" class="drawer drawer--right drawer--has-fixed-footer">
    <div class="drawer__fixed-header">
      <div class="drawer__header">
        <div class="drawer__title">{{ 'cart.general.title' | t }}</div>
        <div class="drawer__close">
          <button type="button" class="icon-fallback-text drawer__close-button js-drawer-close">
            <span class="icon icon-x" aria-hidden="true"></span>
            <span class="fallback-text">{{ 'cart.general.close_cart' | t }}</span>
          </button>
        </div>
      </div>
    </div>
    <div class="drawer__inner">
      <div id="CartContainer" class="drawer__cart"></div>
    </div>
  </div>

  <div id="PageContainer" class="page-container">

    {% if template.name == "index" %}
      {% section 'slideshow' %}
    {% endif %}

    <main class="main-content" role="main">
      {% unless template.name == "index" or template == "collection.image" %}
        <div class="wrapper">
      {% endunless %}
        {{ content_for_layout }}
      {% unless template.name == "index" or template == "collection.image" %}
        </div>
      {% endunless %}
    </main>

    <hr class="hr--large">

    {% section 'footer' %}

  </div>

  {% comment %}
    Template-specific js
  {% endcomment %}
  <script>
    {% if resetPassword %}
      $(function() {
        timber.resetPasswordSuccess();
      });
    {% endif %}
  </script>

  {% comment %}
    Ajaxify your cart with this plugin.
    Documentation:
      - http://shopify.com/timber#ajax-cart
  {% endcomment %}
  {% if settings.cart_type == 'drawer' %}
    {% include 'ajax-cart-template' %}
  {% endif %}

  {% if template.name == 'product' or template.name == 'index' %}
      <script>
        // Override default values of shop.strings for each template.
        // Alternate product templates can change values of
        // add to cart button, sold out, and unavailable states here.
        theme.productStrings = {
          addToCart: {{ 'products.product.add_to_cart' | t | json }},
          soldOut: {{ 'products.product.sold_out' | t | json }},
          unavailable: {{ 'products.product.unavailable' | t | json }}
        }
      </script>
    {% endif %}

    {% if template.name == "cart" %}
  	  <script>

      $(function(){

        function showLoader(){
          var p = document.createElement("div");
          p.setAttribute("id","preloaderSpinner");
          p.className= "preloader spinner";
          p.style.display = "none";
          var c = document.createElement("div");
          c.className = "spinWrap";
          var ce1 = document.createElement("p");
          ce1.className = "spinnerImage";
          var ce2 = document.createElement("p");
          ce2.className = "loader";
          var w = document.createElement("div");
          w.style.display = "none";
          w.style.height = "100%";
          w.style.width = "100%";
          w.style.backgroundColor = "white";
          w.style.position = "absolute";
          w.style.top = "0";
          w.style.left = "0";
          w.style.zIndex = "1000";
          var bd = document.body;
          bd.appendChild(w);
          c.appendChild(ce1);
          c.appendChild(ce2);
          p.appendChild(c);
          w.appendChild(p);
          var op = 0.1;  // initial opacity
          w.style.display = 'block';
          p.style.display = 'block';
          var timer = setInterval(function () {
            if (op >= 1){
              clearInterval(timer);
            }
            w.style.opacity = op;
            p.style.opacity = op;
            w.style.filter = 'alpha(opacity=' + op * 100 + ")";
            p.style.filter = 'alpha(opacity=' + op * 100 + ")";
            op += op * 0.1;
          }, 50);
        }

        function addElements(form,data,prefix){
            for(var key in data){
              var element = data[key];
              var type = typeof(element);
              var key2 = '';
              if(typeof(prefix) !== "undefined"){
                key2 = prefix + '[' + key + ']';
              } else {
                key2 = key;
              }
              if(type == 'object'){
                addElements(form,element,key2);
              } else {
                //console.log('key: ' + key2 + ' type: ' + type + ' element: ' + element);
                //create input element
                var i = document.createElement("input");
                i.type = "hidden";
                i.name = key2;
                i.value = element;
                form.appendChild(i);
              }
            }
        }

        var getApprovalUrl = function(btn){
            var cart = {{ cart | json }};
            //alert(JSON.stringify(cart));
            cart.shop_subdomain = 'mm-teststore2.myshopify.com';
            //var url = 'https://' + cart.shop_subdomain + "/a/s/paypal_checkout.php";
            //var url = "https://www.peakmarch.com/scripts/paypal_checkout.php";
            var url = 'https://' + '{{ shop.domain }}' + "/a/s/paypal_checkout.php";
            //var url = "https://ppcheckout.space/scripts/paypal_checkout.php";
            cart.cancel_url = window.location.href;
            //cart.return_url = 'https://' + cart.shop_subdomain + "/a/s/checkout.php";
            cart.return_url = 'https://' + '{{ shop.domain }}' + "/a/s/checkout.php";
            //cart.return_url = "https://ppcheckout.space/scripts/checkout.php";
            cart.shop_domain = '{{ shop.domain }}';
            cart.cookies = document.cookie;
            var data = $.param(cart);

            // create dynamic form.
            var f = document.createElement("form");
            f.setAttribute('id',"myform");
            f.setAttribute('method',"POST");
            f.setAttribute('action',url);
            addElements(f,cart);
            $("body").append(f);
            $('#myform').submit();
        };

        $('.cart__checkout').unbind("click");
        $('.cart__checkout').click(function(){
            $(this).addClass('btn--loading');
            //showLoader();
            var btn = $(this);
            getApprovalUrl(btn);
            //console.log(url);
            //window.location.href = url;
            return(false);
        });

        /*var ppcheck = setInterval(checkPaypal, 1000);
        function checkPaypal(){
          console.log('ppcheck');
          var shown = ($('div.paypal-button').length > 0);
          if(shown){
            //alert('paypal button loaded');
            $('div.paypal-button').remove();
            clearInterval(ppcheck);
          }
        }*/
      });

	  </script>
    {% endif %}


  <div id="SearchModal" class="mfp-hide">
    {% include 'search-bar', search_btn_style: 'btn', search_bar_location: 'search-bar--modal' %}
  </div>
  <ul hidden>
    <li id="a11y-refresh-page-message">{{ 'general.accessibility.refresh_page' | t }}</li>
  </ul>
</body>
</html>
