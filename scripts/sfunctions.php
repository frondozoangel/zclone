<?php

  require_once("rest.php");
  require_once("db.php");
  function get_shop($store){
    $access_token = getCol("SELECT * FROM tokens WHERE shop = '".$store."' ",'oauth_token');
    $headers =  array(
        'Content-Type: application/json',
        'Host: '.$store,
        'X-Shopify-Access-Token: '.$access_token
    );

    $url = 'https://'.$store.'/admin/api/2019-07/shop.json';
    //echo "<pre>".print_r(compact('checkout_url','data_string','headers'),true)."</pre>"; //die();
    $result = httpGet($url,null,null,$headers);
    //echo $result;
    if($result){
      $result = json_decode($result,true);
      if(isset($result['shop'])){
        $result = $result['shop'];
        //echo "<pre>".print_r($result,true)."</pre>";
      } else {
        echo "<pre>".print_r($result,true)."</pre>"; die();
      }
    }
    //echo "<pre>".print_r(compact('result'),true)."</pre>"; //die();
    return($result);
  }

  /* create checkout in shopify.
  // ref: https://help.shopify.com/en/api/guides/sell-through-the-checkout-api
  */
  function create_shopify_checkout(){
    global $payer,$payee,$items,$shipping,$store;

    $tmp = array();
    foreach($items as $item){
      $variant_id = $item['sku'];
      $quantity = $item['quantity'];
      $fulfillment_service = 'manual';
      $requires_shipping = 0;
      $taxable = 0;
      $tmp[] = compact('variant_id','quantity','fulfillment_service','requires_shipping','taxable');
    }
    $line_items = json_encode($tmp);

    $json = '{
      "checkout":{
        "email": "'.$payer['email_address'].'",
        "line_items": '.$line_items.',
        "billing_address": {
          "first_name": "'.$payer['name']['given_name'].'",
          "last_name": "'.$payer['name']['surname'].'",
          "address1": "'.$shipping['address']['address_line_1'].'",
          "city": "'.$shipping['address']['admin_area_2'].'",
          "country_code": "'.$shipping['address']['country_code'].'",
          "province_code": "'.$shipping['address']['admin_area_1'].'",
          "phone": "'.(isset($payer['phone']) ? $payer['phone'] : '').'",
          "zip": "'.$shipping['address']['postal_code'].'"
        },
        "shipping_address": {
          "first_name": "'.$payer['name']['given_name'].'",
          "last_name": "'.$payer['name']['surname'].'",
          "address1": "'.$shipping['address']['address_line_1'].'",
          "city": "'.$shipping['address']['admin_area_2'].'",
          "country_code": "'.$shipping['address']['country_code'].'",
          "province_code": "'.$shipping['address']['admin_area_1'].'",
          "phone": "'.(isset($payer['phone']) ? $payer['phone'] : '').'",
          "zip": "'.$shipping['address']['postal_code'].'"
        }
      }
    }';

    $access_token = getCol("SELECT * FROM tokens WHERE shop = '".$store."' ",'oauth_token');
    $data2 = json_decode($json,true);
    //echo "<pre>".print_r($data2,true)."</pre>";
    $data_string = json_encode($data2);
    $headers =  array(
        'Content-Type: application/json',
        'Host: '.$store,
        'X-Shopify-Access-Token: '.$access_token
    );

    $checkout_url = 'https://'.$store.'/admin/checkouts.json';
    //echo "<pre>".print_r(compact('checkout_url','data_string','headers'),true)."</pre>"; //die();
    $result = httpPost($checkout_url,$data2,null,null,true,$headers);
    //echo $result;
    if($result){
      $result = json_decode($result,true);
      if(isset($result['checkout'])){
        $result = $result['checkout'];
        //echo "<pre>".print_r($result,true)."</pre>";
      } else {
        echo "<pre>".print_r($result,true)."</pre>"; die();
      }
    }
    //echo "<pre>".print_r(compact('result'),true)."</pre>"; //die();
    return($result);
  }

  /* poll shipping rates in shopify.
  // ref: https://help.shopify.com/en/api/guides/sell-through-the-checkout-api#polling-for-shipping-rates
  */
  function poll_shopify_shipping($token){
    global $payer,$payee,$items,$shipping,$store;

    $access_token = getCol("SELECT * FROM tokens WHERE shop = '".$store."' ",'oauth_token');
    $headers =  array(
        'Content-Type: application/json',
        'Host: '.$store,
        'X-Shopify-Access-Token: '.$access_token
    );

    $checkout_url = 'https://'.$store.'/admin/checkouts/'.$token.'/shipping_rates.json';
    //echo "<pre>".print_r(compact('checkout_url','data_string','headers'),true)."</pre>"; //die();
    $result = httpGet($checkout_url,null,null,$headers);
    //echo $result;
    if($result){
      $result = json_decode($result,true);
      if(isset($result['shipping_rates'])){
        $result = $result['shipping_rates'];
      }
    }
    //echo "<pre>".print_r(compact('result'),true)."</pre>"; //die();
    return($result);
  }

  /* update checkout in shopify.
  // ref: https://help.shopify.com/en/api/guides/sell-through-the-checkout-api
  */
  function update_shopify_checkout($token,$json){
    global $payer,$payee,$items,$shipping,$store,$shipping_rates;

    $access_token = getCol("SELECT * FROM tokens WHERE shop = '".$store."' ",'oauth_token');
    $data2 = json_decode($json,true);
    //echo "<pre>".print_r($data2,true)."</pre>";
    $data_string = json_encode($data2);
    $headers =  array(
        'Content-Type: application/json',
        'Host: '.$store,
        'X-Shopify-Access-Token: '.$access_token
    );

    $checkout_url = 'https://'.$store.'/admin/checkouts/'.$token.'.json';
    //echo "<pre>".print_r(compact('checkout_url','data_string','headers'),true)."</pre>"; //die();
    $result = httpPut($checkout_url,$data2,null,null,true,$headers);
    //echo $result;
    if($result){
      $result = json_decode($result,true);
      if(isset($result['checkout'])){
        $result = $result['checkout'];
        //echo "<pre>".print_r($result,true)."</pre>";
      } else {
        echo "<pre>".print_r($result,true)."</pre>"; die();
      }
    }
    //echo "<pre>".print_r(compact('result'),true)."</pre>"; //die();
    return($result);
  }

  /* complete checkout in shopify.
  // ref: https://help.shopify.com/en/api/guides/sell-through-the-checkout-api#completing-the-purchase
  */
  function complete_shopify_checkout($token){
    global $payer,$payee,$items,$shipping,$store;

    $json = '{
      "checkout": {
        "token": "'.$token.'",
        "order": null
      }
    }';

    $access_token = getCol("SELECT * FROM tokens WHERE shop = '".$store."' ",'oauth_token');
    $data2 = json_decode($json,true);
    //echo "<pre>".print_r($data2,true)."</pre>";
    $data_string = json_encode($data2);
    $headers =  array(
        'Content-Type: application/json',
        'Host: '.$store,
        'X-Shopify-Access-Token: '.$access_token
    );

    $checkout_url = 'https://'.$store.'/admin/checkouts/'.$token.'/complete.json';
    //echo "<pre>".print_r(compact('checkout_url','data_string','headers'),true)."</pre>"; //die();
    $result = httpPost($checkout_url,$data2,null,null,true,$headers);
    //echo $result;
    if($result){
      $result = json_decode($result,true);
      if(isset($result['checkout'])){
        $result = $result['checkout'];
      } else {
        echo "<pre>".print_r($result,true)."</pre>"; die();
      }
    }
    //echo "<pre>".print_r(compact('result'),true)."</pre>"; die();
    return($result);
  }

  /* create checkout in shopify.
  // ref: https://help.shopify.com/en/api/reference/orders/draftorder?api[version]=2019-07#create-2019-07
  */
  function create_shopify_draft_order(){
    global $payer,$payee,$items,$shipping,$store, $shipping_cost;

    $tmp = array();
    foreach($items as $item){
      $variant_id = $item['sku'];
      $quantity = $item['quantity'];
      $fulfillment_service = 'manual';
      $requires_shipping = 0;
      $taxable = 0;
      $tmp[] = compact('variant_id','quantity','fulfillment_service','requires_shipping','taxable');
    }
    $line_items = json_encode($tmp);
    //echo "<pre>".print_r($payer,true)."</pre>";
    $payer_phone = '';
    if(isset($payer['phone'])){
      if(isset($payer['phone']['phone_number'])){
        if(isset($payer['phone']['phone_number']['national_number'])){
          $payer_phone = $payer['phone']['phone_number']['national_number'];
        }
      }
    }

    $json = '{
      "draft_order":{
        "email": "'.$payer['email_address'].'",
        "line_items": '.$line_items.',
        "billing_address": {
          "first_name": "'.$payer['name']['given_name'].'",
          "last_name": "'.$payer['name']['surname'].'",
          "address1": "'.$shipping['address']['address_line_1'].'",
          "city": "'.(isset($shipping['address']['admin_area_2']) ? $shipping['address']['admin_area_2'] : '').'",
          "country": "'.$shipping['address']['country_code'].'",
          "country_code": "'.$shipping['address']['country_code'].'",
          "province": "'.(isset($shipping['address']['admin_area_1']) ? $shipping['address']['admin_area_1'] : '').'",
          "province_code": "'.(isset($shipping['address']['admin_area_1']) ? $shipping['address']['admin_area_1'] : '').'",
          "phone": "'.$payer_phone.'",
          "zip": "'.$shipping['address']['postal_code'].'"
        },
        "shipping_address": {
          "first_name": "'.$payer['name']['given_name'].'",
          "last_name": "'.$payer['name']['surname'].'",
          "address1": "'.$shipping['address']['address_line_1'].'",
          "city": "'.(isset($shipping['address']['admin_area_2']) ? $shipping['address']['admin_area_2'] : '').'",
          "country": "'.$shipping['address']['country_code'].'",
          "country_code": "'.$shipping['address']['country_code'].'",
          "province": "'.(isset($shipping['address']['admin_area_1']) ? $shipping['address']['admin_area_1'] : '').'",
          "province_code": "'.(isset($shipping['address']['admin_area_1']) ? $shipping['address']['admin_area_1'] : '').'",
          "phone": "'.$payer_phone.'",
          "zip": "'.$shipping['address']['postal_code'].'"
        },
        "shipping_line": {
          "custom": true,
          "price": '.$shipping_cost.',
          "title": "Standard Shipping"
        }
      }
    }';

    $access_token = getCol("SELECT * FROM tokens WHERE shop = '".$store."' ",'oauth_token');
    $data2 = json_decode($json,true);
    //echo "<pre>".print_r($data2,true)."</pre>";
    $data_string = json_encode($data2);
    $headers =  array(
        'Content-Type: application/json',
        'Host: '.$store,
        'X-Shopify-Access-Token: '.$access_token
    );

    $checkout_url = 'https://'.$store.'/admin/api/2019-07/draft_orders.json';
    //echo "<pre>".print_r(compact('checkout_url','data_string','headers'),true)."</pre>"; //die();
    $result = httpPost($checkout_url,$data2,null,null,true,$headers);
    //echo $result;
    if($result){
      $result = json_decode($result,true);
      if(isset($result['draft_order'])){
        $result = $result['draft_order'];
        $line_items = $result['line_items'];
        //$products = array();
        foreach($line_items as $key => $line_item){
          $product = get_shopify_product($line_item['product_id']);
          //echo "<pre>".print_r($product,true)."</pre>";
          $line_item['image_url'] = "//cdn.shopify.com/s/assets/checkout/product-blank-98d4187c2152136e9fb0587a99dfcce6f6873f3a9f21ea9135ed7f495296090f.png";
          if(isset($product['product']['image'])){
            if(isset($product['product']['image']['src'])){
              $line_item['image_url'] = $product['product']['image']['src'];
            }
          }
          $line_items[$key] = $line_item;
        }
        //echo "<pre>".print_r($products,true)."</pre>";
        $result['line_items'] = $line_items;
      }
    }
    //echo "<pre>".print_r(compact('result'),true)."</pre>"; //die();
    return($result);
  }

  /* update draft order in shopify.
  // ref: https://help.shopify.com/en/api/reference/orders/draftorder#update-2019-07
  */
  function update_shopify_draft_order($token,$json){
    global $payer,$payee,$items,$shipping,$store,$shipping_rates;

    $access_token = getCol("SELECT * FROM tokens WHERE shop = '".$store."' ",'oauth_token');
    $data2 = json_decode($json,true);
    //echo "<pre>".print_r($data2,true)."</pre>";
    $data_string = json_encode($data2);
    $headers =  array(
        'Content-Type: application/json',
        'Host: '.$store,
        'X-Shopify-Access-Token: '.$access_token
    );

    $checkout_url = 'https://'.$store.'/admin/api/2019-07/draft_orders/'.$token.'.json';
    //echo "<pre>".print_r(compact('checkout_url','data_string','headers'),true)."</pre>"; //die();
    $result = httpPut($checkout_url,$data2,null,null,true,$headers);
    //echo $result;
    if($result){
      $result = json_decode($result,true);
    }
    //echo "<pre>".print_r(compact('result'),true)."</pre>"; //die();
    return($result);
  }

  /* complete draft order in shopify.
  // ref: https://help.shopify.com/en/api/reference/orders/draftorder?api[version]=2019-07#complete-2019-07
  */
  function complete_shopify_draft_order($token){
    global $payer,$payee,$items,$shipping,$store;

    $access_token = getCol("SELECT * FROM tokens WHERE shop = '".$store."' ",'oauth_token');
    $headers =  array(
        'Content-Type: application/json',
        'Host: '.$store,
        'X-Shopify-Access-Token: '.$access_token
    );

    $checkout_url = 'https://'.$store.'/admin/api/2019-07/draft_orders/'.$token.'/complete.json';
    //echo "<pre>".print_r(compact('checkout_url','data_string','headers'),true)."</pre>"; //die();
    $result = httpPut($checkout_url,array(),null,null,true,$headers);
    //echo $result;
    if($result){
      $result = json_decode($result,true);
      if(isset($result['draft_order'])){
        $result = $result['draft_order'];
      }
    }
    //echo "<pre>".print_r(compact('result'),true)."</pre>"; die();
    return($result);
  }

  function complete_shopify_draft_order_pending($token){
    global $payer,$payee,$items,$shipping,$store;

    $access_token = getCol("SELECT * FROM tokens WHERE shop = '".$store."' ",'oauth_token');
    $headers =  array(
        'Content-Type: application/json',
        'Host: '.$store,
        'X-Shopify-Access-Token: '.$access_token
    );

    $checkout_url = 'https://'.$store.'/admin/api/2019-07/draft_orders/'.$token.'/complete.json?payment_pending=true';
    //echo "<pre>".print_r(compact('checkout_url','data_string','headers'),true)."</pre>"; //die();
    $result = httpPut($checkout_url,array(),null,null,true,$headers);
    //echo $result;
    if($result){
      $result = json_decode($result,true);
    }
    //echo "<pre>".print_r(compact('result'),true)."</pre>"; die();
    return($result);
  }

  /* get products in shopify.
  // ref: https://help.shopify.com/en/api/reference/products/product#show-2019-07
  */
  function get_shopify_product($token){
    global $payer,$payee,$items,$shipping,$store;

    $access_token = getCol("SELECT * FROM tokens WHERE shop = '".$store."' ",'oauth_token');
    $headers =  array(
        'Content-Type: application/json',
        'Host: '.$store,
        'X-Shopify-Access-Token: '.$access_token
    );

    $url = 'https://'.$store.'/admin/products/'.$token.'.json';
    //echo "<pre>".print_r(compact('url','data_string','headers'),true)."</pre>"; //die();
    $result = httpGet($url,null,null,$headers);
    //echo $result;
    if($result){
      $result = json_decode($result,true);
    }
    //echo "<pre>".print_r(compact('result'),true)."</pre>"; die();
    return($result);
  }

  /* get draft orders in shopify.
  // ref: https://help.shopify.com/en/api/reference/orders/draftorder?api[version]=2019-07#show-2019-07
  */
  function get_shopify_draft_order($token){
    global $payer,$payee,$items,$shipping,$store;

    $access_token = getCol("SELECT * FROM tokens WHERE shop = '".$store."' ",'oauth_token');
    $headers =  array(
        'Content-Type: application/json',
        'Host: '.$store,
        'X-Shopify-Access-Token: '.$access_token
    );

    $url = 'https://'.$store.'/admin/api/2019-07/draft_orders/'.$token.'.json';
    //echo "<pre>".print_r(compact('url','data_string','headers'),true)."</pre>"; //die();
    $result = httpGet($url,null,null,$headers);
    //echo $result;
    if($result){
      $result = json_decode($result,true);
      if(isset($result['draft_order'])){
        $result = $result['draft_order'];
        $line_items = $result['line_items'];
        //$products = array();
        foreach($line_items as $key => $line_item){
          $product = get_shopify_product($line_item['product_id']);
          //echo "<pre>".print_r($product,true)."</pre>";
          $line_item['image_url'] = "//cdn.shopify.com/s/assets/checkout/product-blank-98d4187c2152136e9fb0587a99dfcce6f6873f3a9f21ea9135ed7f495296090f.png";
          if(isset($product['product']['image'])){
            if(isset($product['product']['image']['src'])){
              $line_item['image_url'] = $product['product']['image']['src'];
            }
          }
          $line_items[$key] = $line_item;
        }
        //echo "<pre>".print_r($products,true)."</pre>";
        $result['line_items'] = $line_items;
      }
    }
    //echo "<pre>".print_r(compact('result'),true)."</pre>"; die();
    return($result);
  }

  /* get orders in shopify.
  // ref: https://help.shopify.com/en/api/reference/orders/order?api[version]=2019-07#show-2019-07
  */
  function get_shopify_order($token){
    global $payer,$payee,$items,$shipping,$store;

    $access_token = getCol("SELECT * FROM tokens WHERE shop = '".$store."' ",'oauth_token');
    $headers =  array(
        'Content-Type: application/json',
        'Host: '.$store,
        'X-Shopify-Access-Token: '.$access_token
    );

    $url = 'https://'.$store.'/admin/api/2019-07/orders/'.$token.'.json';
    //echo "<pre>".print_r(compact('url','data_string','headers'),true)."</pre>"; //die();
    $result = httpGet($url,null,null,$headers);
    //echo $result;
    if($result){
      $result = json_decode($result,true);
      if(isset($result['order'])){
        $result = $result['order'];
      }
    }
    //echo "<pre>".print_r(compact('result'),true)."</pre>"; die();
    return($result);
  }

  /* create checkout in shopify.
  // ref: https://help.shopify.com/en/api/reference/orders/draftorder?api[version]=2019-07#create-2019-07
  */
  function create_shopify_pending_order(){
    global $payer,$payee,$items,$shipping,$store;

    $tmp = array();
    foreach($items as $item){
      $variant_id = $item['sku'];
      $quantity = $item['quantity'];
      $fulfillment_service = 'manual';
      $requires_shipping = 0;
      $taxable = 0;
      $tmp[] = compact('variant_id','quantity','fulfillment_service','requires_shipping','taxable');
    }
    $line_items = json_encode($tmp);

    $json = '{
      "order":{
        "email": "'.$payer['email_address'].'",
        "line_items": '.$line_items.',
        "billing_address": {
          "first_name": "'.$payer['name']['given_name'].'",
          "last_name": "'.$payer['name']['surname'].'",
          "address1": "'.$shipping['address']['address_line_1'].'",
          "city": "'.$shipping['address']['admin_area_2'].'",
          "country_code": "'.$shipping['address']['country_code'].'",
          "province_code": "'.$shipping['address']['admin_area_1'].'",
          "phone": "'.(isset($payer['phone']) ? $payer['phone'] : '').'",
          "zip": "'.$shipping['address']['postal_code'].'"
        },
        "shipping_address": {
          "first_name": "'.$payer['name']['given_name'].'",
          "last_name": "'.$payer['name']['surname'].'",
          "address1": "'.$shipping['address']['address_line_1'].'",
          "city": "'.$shipping['address']['admin_area_2'].'",
          "country_code": "'.$shipping['address']['country_code'].'",
          "province_code": "'.$shipping['address']['admin_area_1'].'",
          "phone": "'.(isset($payer['phone']) ? $payer['phone'] : '').'",
          "zip": "'.$shipping['address']['postal_code'].'"
        },
        "shipping_line": {
          "custom": true,
          "price": 4.95,
          "title": "Standard Shipping"
        }
      }
    }';

    $access_token = getCol("SELECT * FROM tokens WHERE shop = '".$store."' ",'oauth_token');
    $data2 = json_decode($json,true);
    //echo "<pre>".print_r($data2,true)."</pre>";
    $data_string = json_encode($data2);
    $headers =  array(
        'Content-Type: application/json',
        'Host: '.$store,
        'X-Shopify-Access-Token: '.$access_token
    );

    $checkout_url = 'https://'.$store.'/admin/api/2019-07/orders.json';
    //echo "<pre>".print_r(compact('checkout_url','data_string','headers'),true)."</pre>"; //die();
    $result = httpPost($checkout_url,$data2,null,null,true,$headers);
    //echo $result;
    if($result){
      $result = json_decode($result,true);
      if(isset($result['order'])){
        $result = $result['order'];
        $line_items = $result['line_items'];
        //$products = array();
        foreach($line_items as $key => $line_item){
          $product = get_shopify_product($line_item['product_id']);
          //echo "<pre>".print_r($product,true)."</pre>";
          $line_item['image_url'] = "//cdn.shopify.com/s/assets/checkout/product-blank-98d4187c2152136e9fb0587a99dfcce6f6873f3a9f21ea9135ed7f495296090f.png";
          if(isset($product['product']['image'])){
            if(isset($product['product']['image']['src'])){
              $line_item['image_url'] = $product['product']['image']['src'];
            }
          }
          $line_items[$key] = $line_item;
        }
        //echo "<pre>".print_r($products,true)."</pre>";
        $result['line_items'] = $line_items;
      }
    }
    //echo "<pre>".print_r(compact('result'),true)."</pre>"; die();
    return($result);
  }

  /* update order in shopify.
  // ref: https://help.shopify.com/en/api/reference/orders/order#update-2019-07
  */
  function update_shopify_order($token,$json){
    global $payer,$payee,$items,$shipping,$store,$shipping_rates;

    $access_token = getCol("SELECT * FROM tokens WHERE shop = '".$store."' ",'oauth_token');
    $data2 = json_decode($json,true);
    //echo "<pre>".print_r($data2,true)."</pre>";
    $data_string = json_encode($data2);
    $headers =  array(
        'Content-Type: application/json',
        'Host: '.$store,
        'X-Shopify-Access-Token: '.$access_token
    );

    $checkout_url = 'https://'.$store.'/admin/api/2019-07/orders/'.$token.'.json';
    //echo "<pre>".print_r(compact('checkout_url','data_string','headers'),true)."</pre>"; //die();
    $result = httpPut($checkout_url,$data2,null,null,true,$headers);
    //echo $result;
    if($result){
      $result = json_decode($result,true);
    }
    //echo "<pre>".print_r(compact('result'),true)."</pre>"; //die();
    return($result);
  }

  /* update order in shopify.
  // ref: https://help.shopify.com/en/api/reference/orders/order#update-2019-07
  */
  function cancel_shopify_order($token,$json){
    global $payer,$payee,$items,$shipping,$store,$shipping_rates;

    $access_token = getCol("SELECT * FROM tokens WHERE shop = '".$store."' ",'oauth_token');
    $data2 = json_decode($json,true);
    //echo "<pre>".print_r($data2,true)."</pre>";
    $data_string = json_encode($data2);
    $headers =  array(
        'Content-Type: application/json',
        'Host: '.$store,
        'X-Shopify-Access-Token: '.$access_token
    );

    $checkout_url = 'https://'.$store.'/admin/api/2019-07/orders/'.$token.'/cancel.json';
    //echo "<pre>".print_r(compact('checkout_url','data_string','headers'),true)."</pre>"; //die();
    $result = httpPost($checkout_url,$data2,null,null,true,$headers);
    //echo $result;
    if($result){
      $result = json_decode($result,true);
    }
    //echo "<pre>".print_r(compact('result'),true)."</pre>"; //die();
    return($result);
  }
?>