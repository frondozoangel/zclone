<style type="text/css">
    .loadSpinner {
       height: 70px;
       width: 70px;
       border-radius: 50%;
       border: 4px solid rgba(255,255,255,0);
       border-top-color: 4px solid #A0522D;
       border-right-color: 4px solid #A0522D;
       -webkit-animation: single2 2s infinite linear;
       animation: single2 2s infinite linear;
       margin: 0 auto;
       margin-bottom: 25px !important;
    }
    @-webkit-keyframes single2 {
       0% {
          -webkit-transform: rotate(0deg);
          transform: rotate(0deg);
          border-top-color: #A0522D;
          border-right-color: #A0522D;
       }
       50% {
          border-top-color: #1f4f58;
          border-right-color: #1f4f58;
       }
       100% {
          -webkit-transform: rotate(720deg);
          transform: rotate(720deg);
          border-top-color: #A0522D;
          border-right-color: #A0522D;
       }
    }
    @keyframes single2 {
       0% {
          -webkit-transform: rotate(0deg);
          transform: rotate(0deg);
          border-top-color: #A0522D;
          border-right-color: #A0522D;
       }
       50% {
          border-top-color: #A0522D;
          border-right-color: #A0522D;
       }
       100% {
          -webkit-transform: rotate(720deg);
          transform: rotate(720deg);
          border-top-color: #A0522D;
          border-right-color: #A0522D;
       }
    }
    .spinnerWrapper {
       width: 1000px;
       position: fixed;
       left: 50%;
       top: 25%;
       transform: translate(-50%, 0%)
    }
    .spinnerMessage {
      font-family: Arial, Helvetica, sans-serif;
      text-align: center;
      color: #595959;
    }
    .spinnerMessage div {
      margin-top: 5px;
    }
    .refreshLink {
      color: #CD853F;
      text-decoration: none;
    }
    #wrapper {
      position:fixed;
      padding:0;
      margin:0;
      top:0;
      left:0;
      width: 100%;
      height: 100%;
      background:rgba(255,255,255);
      z-index: 800;
    }
    /* ----------- iPhone 5 ----------- */
    @media only screen 
      and (min-device-width: 320px) 
      and (max-device-width: 568px)
      and (-webkit-min-device-pixel-ratio: 2) {
        .spinnerWrapper{
          width: 300px;
        }
      .spinnerMessage {
        font-size: 14px;
      }
    }
    /* ----------- iPhone 6, 6S, 7 and 8 ----------- */
        @media only screen 
          and (min-device-width: 375px) 
          and (max-device-width: 667px) 
          and (-webkit-min-device-pixel-ratio: 2) { 
      .spinnerMessage {
        font-size: 14px;
      }
        }
        /* ----------- Galaxy S4, S5 and Note 3 ----------- */
        @media screen 
      and (device-width: 320px) 
      and (device-height: 640px) 
      and (-webkit-device-pixel-ratio: 3) {
      .spinnerMessage {
        font-size: 16px;
      }
    }
    @media only screen 
    and (min-width: 1024px) 
    and (max-height: 1366px) 
    and (-webkit-min-device-pixel-ratio: 1.5) {
      .spinnerMessage {
        font-size: 22px;
      }
    }
    @media only screen 
    and (min-device-width : 768px) 
    and (max-device-width : 1024px) {
      .spinnerMessage {
        font-size: 16px;
      }
    }
</style>
<script>
  //showLoader();
  function showLoader(){
      var w = document.createElement("div");
      w.setAttribute("id","wrapper");
      var sw = document.createElement("div");
      sw.className = "spinnerWrapper";
      var ls = document.createElement("div");
      ls.className = "loadSpinner";
      var sm = document.createElement("div");
      sm.className = "spinnerMessage";
      var sp = document.createElement("span");
      sp.innerHTML = "Your order is being processed."
      var m = document.createElement("div");
      m.innerHTML = "If you are not automatically redirected, <span class=\"refreshLink\">refresh this page</span>."

      document.body.append(w);
      w.append(sw);
      sw.append(ls);
      sw.append(sm);
      sm.append(sp);
      sm.append(m);


      /*var op = 0.1;  // initial opacity
      var timer = setInterval(function () {
      if (op >= 1){
        clearInterval(timer);
      }
      w.style.display = 'block';
      w.style.opacity = op;
      w.style.filter = 'alpha(opacity=' + op * 100 + ")";
      op += op * 0.1;
      }, 155);*/
  }
</script>