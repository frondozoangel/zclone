<?php

  require_once("db.php");
  function decrypt($str){
    $response = httpGet("https://app1.peakmarch.com/decrypt?str=".$str);
    //echo "<pre>".print_r($response,true)."</pre>";
    if($response){
      $response = json_decode($response,true);
      if(isset($response['decrypted'])){
        return($response['decrypted']);
      }
    }
    return($response);
  }

  get_paypal_settings();
  function get_paypal_settings(){
    global $settings,$data,$access_token_url,$payment_url,$client_id,$client_secret,$get_order_url,$get_payment_url,$header_script,$force_payment_capture,$shipping_cost,$delay_timer,$webhook_uri;
    if(isset($data['shop_subdomain'])){
      $settings = getRow("select s.* FROM settings s, stores st WHERE s.store_id = st.id AND st.domain =  '".$data['shop_subdomain']."'");
      //echo "<pre>".print_r($settings,true)."</pre>"; die();
      if($settings['paypal_mode'] == '2'){
        $mode = "sandbox";
        $access_token_url = "https://api.sandbox.paypal.com/v1/oauth2/token";
        $payment_url = "https://api.sandbox.paypal.com/v2/checkout/orders";
        //$client_id = "AYeZEQmIgep53U6rIA14JMvf5MtZMqH-9O1BiYXDldcmdkh19buf9iRYJhSYLEoYIghaEyvFLniaS9kk";
        $client_id = decrypt($settings['sb_client_id']);
        //$client_secret = "EK7aQibcJGiCkRHSdkKd1KV4ptNc8HpgvgUNA41gW5gGLuwEMGTJL5e55yceCADU2wKHhyrsy5bof_Fl";
        $client_secret = decrypt($settings['sb_client_secret']);
        $get_order_url = "https://api.sandbox.paypal.com/v2/checkout/orders/{order_id}";
        $get_payment_url = "https://api.sandbox.paypal.com/v2/payments/authorizations/{authorization_id}";
      } else {
        $mode = "live";
        $access_token_url = "https://api.paypal.com/v1/oauth2/token";
        $payment_url = "https://api.paypal.com/v2/checkout/orders";
        //$client_id = "AQoxYlKqRmBEn1DjtiyYLWhqy8La_Y2UFGSnAMadUae_Amhwyx0_e19q3sVcTHnRWQ_GRY-lbXjs54Rd";
        $client_id = decrypt($settings['live_client_id']);
        //$client_secret = "EPskmyplrjpihQQb_X04rqld35RAVn3Irt4AWC0jpP9vtyZC7L100R6ch7zdXXnIuUoiakCRMsPk86fL";
        $client_secret = decrypt($settings['live_client_secret']);
        $get_order_url = "https://api.paypal.com/v2/checkout/orders/{order_id}";
        $get_payment_url = "https://api.paypal.com/v2/payments/authorizations/{authorization_id}";
      }
    }

    $header_script = "";
    if(isset($settings['header_script'])){
      $header_script = $settings['header_script'];
    }

    $force_payment_capture = true;
    if(isset($settings['force_payment'])){
      $force_payment_capture = ($settings['force_payment'] == 1);
    }

    $delay_timer = 10;
    if(isset($settings['delay_timer'])){
      if($settings['delay_timer'] > 0){
        $delay_timer = $settings['delay_timer'];
      }
    }

    $shipping_cost = 4.95;
    if(isset($settings['shipping_cost'])){
      $shipping_cost = $settings['shipping_cost'];
    }

    $webhook_uri = '';
    if(isset($settings['webhook_uri'])){
      $webhook_uri = $settings['webhook_uri'];
    }

  }


?>