<?php
    date_default_timezone_set("UTC");
    //echo "<pre>".print_r(compact('payer','payee','items','shipping','store','order'),true)."</pre>"; //die();
    $cookies = array(
      'date' => date("Y-m-d"),
      'time' => date("H:i:s"),
      'order_id' => $checkout["name"],
      'name' => trim($payer['name']['given_name'].' '.$payer['name']['surname']),
      'email' => $payer['email_address'],
      'phone_number' => $payer_phone,
      'address' => $address1." ".$city." ".$state." ".$zip.", ".$country,
      'no_of_products' => count($line_items),
      'total_value' => ($items_subtotal + $shipping_cost),
      'product_revenue' => $items_subtotal,
      'shipping_cost' => $shipping_cost,
    );

    $total_qty = 0;
    $quantities = array();
    $product_names = explode(",","A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z");
    foreach($line_items as $item_index => $item){
      $qty = $item['quantity'];
      $total_qty += $qty;
      $product_name = '';
      $x = $item_index;
      //$x = 30;
      $max = count($product_names);
      if($x < $max){
        $product_name .= $product_names[$x];
      } else {
        while($x >= $max){
          $product_name .= $product_names[($max-1)];
          $x -= $max;
          //echo "<pre>".print_r(compact('x','max'),true)."</pre>";
          //break;
        }
        $product_name .= $product_names[$x];
      }
      $cookies['product'.$product_name.'_qty'] = $qty;
      $cookies['product'.$product_name.'_name'] = $item['title'];
    }
    $cookies['no_of_products'] = $total_qty;

    $row = getRow("SELECT cookies FROM checkouts WHERE paypal_token = '".$data['token']."' ");
    $tmp = urldecode($row['cookies']);
    //echo "<pre>".print_r($cookies,true)."</pre>"; die();
    $rows = explode(";",$tmp);
    foreach($rows as $row){
      $cparts = explode("=",$row);
      $remove_list = "_ab,_shopify_y,_gcl_au,_y,_shopify_fs,_fbp,_ga,_gid,_s,_shopify_s,_shopify_sa_p,shopify_pay_redirect,_shopify_sa_t";
      $name = trim($cparts[0]);
      $value = trim($cparts[1]);
      if(!in_array($name,explode(",",$remove_list))){
        $cookies[$name] = $value;
      }
    }

    /*foreach($cookies as $name => $value){
        setcookie($name, $value, time() + (86400 * 30), "/"); // 86400 = 1 day
    }*/

    //echo "<pre>".print_r(compact('cookies'),true)."</pre>";
    //echo "<pre>".print_r($_COOKIE,true)."</pre>";
?>

<script>
    // create post to webhook uri.
    var url = '<?php echo $webhook_uri; ?>';
    console.log('webhook_uri: ' + url);
    var params = {};
    // add cookies.
    <?php
    foreach($cookies as $name => $value){ ?>
        document.cookie = '<?php echo $name; ?>=<?php echo $value;?>; expires=<?php echo date(DATE_RFC822,(time() + (86400 * 30))); ?>; path=/';
        params['<?php echo $name; ?>'] = '<?php echo $value; ?>';
    <?php } ?>
    console.log(params);
    var data = $.param(params);
    $.post( url, data, function( response ) {
      console.log(response);
    });
</script>
