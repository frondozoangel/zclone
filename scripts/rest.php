<?php

function httpDelete($url,$params,$username = null,$password = null,$json = false)
{
  $postData = '';
   //create name value pairs seperated by &
   foreach($params as $k => $v)
   {
      $postData .= $k . '='.$v.'&';
   }
   $postData = rtrim($postData, '&');

    $ch = curl_init();

    curl_setopt($ch,CURLOPT_URL,$url);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
    curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
    curl_setopt($ch,CURLOPT_HEADER, false);
    //curl_setopt($ch, CURLOPT_POST, count($postData));
    //curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);

    if(isset($username)){
      curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
      curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");
    }
    if($json){
      $data_string = json_encode($params);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_HTTPHEADER, array(
          'Content-Type: application/json',
          'Content-Length: ' . strlen($data_string))
      );
    }

    $output=curl_exec($ch);

    curl_close($ch);
    return $output;

}

function httpPost($url,$params,$username = null,$password = null,$json = false,$headers = array())
{

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL,$url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER,true);

    if($json){
      // send payments request as json.
      // ref: https://lornajane.net/posts/2011/posting-json-data-with-php-curl
      $data_string = '';
      if(!empty($params)){
        $data_string = json_encode($params);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
      }
      curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
      if(empty($headers)){
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($data_string))
        );
      }
    } else {
      $postData = http_build_query($params);
      curl_setopt($ch, CURLOPT_POST, true);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
    }

    if(isset($username)){
      curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");
    }

    if(!empty($headers)){
      curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    }

    $output=curl_exec($ch);
    if(curl_errno($ch)){
        //If an error occured, throw an Exception.
        throw new Exception(curl_error($ch));
    }

    curl_close($ch);
    return $output;

}

function httpPut($url,$params,$username = null,$password = null,$json = false,$headers = array())
{

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL,$url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER,true);

    if($json){
      // send payments request as json.
      // ref: https://lornajane.net/posts/2011/posting-json-data-with-php-curl
      $data_string = json_encode($params);
      curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
      curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
      if(empty($headers)){
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($data_string))
        );
      }
    } else {
      $postData = http_build_query($params);
      curl_setopt($ch, CURLOPT_POST, true);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
    }

    if(isset($username)){
      curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");
    }

    if(!empty($headers)){
      curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    }

    $output=curl_exec($ch);
    if(curl_errno($ch)){
        //If an error occured, throw an Exception.
        throw new Exception(curl_error($ch));
    }

    curl_close($ch);
    return $output;

}

function httpPatch($url,$params,$username = null,$password = null,$json = false,$headers = array())
{

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL,$url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER,true);

    if($json){
      // send payments request as json.
      // ref: https://lornajane.net/posts/2011/posting-json-data-with-php-curl
      $data_string = json_encode($params);
      curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PATCH");
      curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
      if(empty($headers)){
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($data_string))
        );
      }
    } else {
      $postData = http_build_query($params);
      curl_setopt($ch, CURLOPT_POST, true);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
    }

    if(isset($username)){
      curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");
    }

    if(!empty($headers)){
      curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    }

    $output=curl_exec($ch);
    if(curl_errno($ch)){
        //If an error occured, throw an Exception.
        throw new Exception(curl_error($ch));
    }

    curl_close($ch);
    return $output;

}

function httpGet($url,$username = null,$password = null,$headers = array())
{
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL,$url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER,true);

    if(isset($username)){
      curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");
    }

    if(!empty($headers)){
      curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    }

    $output=curl_exec($ch);
    if(curl_errno($ch)){
        //If an error occured, throw an Exception.
        throw new Exception(curl_error($ch));
    }

    curl_close($ch);
    return $output;
}

?>