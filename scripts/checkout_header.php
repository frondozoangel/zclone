<?php

  /*$header_script = "<!-- Google Tag Manager -->
      <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
      new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
      j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
      'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
      })(window,document,'script','dataLayer','GTM-KDDL4C3');</script>
      <!-- End Google Tag Manager -->";*/

  // add data layer force_payment capture event.
  //$force_payment_capture = true;
  $header_script .= "<script>
        window.dataLayer = window.dataLayer || [];
        window.dataLayer.push({
         'event': 'Force_Payment_Capture',
         'status': '".($force_payment_capture ? "yes" : "no")."'
        });
      </script>
      ";

  // add data layer transaction event.
  $skus = array();
  $contents = array();
  $total = 0;
  //echo "<pre>".print_r($line_items,true)."</pre>";
  foreach($line_items as $line_item){
    $id = $line_item['product_id'];
    $quantity = $line_item['quantity'];
    $price = $line_item['price'];
    $skus[] = $id;
    $contents[] = compact('id','quantity','price');
    $total += ($price * $quantity);
  }

  $json = '{
          "event": "TransactionSuccess",
          "shipping": '.$shipping_cost.',
          "total_revenue": '.($shipping_cost + $total).',
          "customDataFB": {
            "content_ids": '.json_encode($skus).',
            "content_type": "product",
            "contents" : '.json_encode($contents).',
            "value" : '.$total.',
            "currency" : "USD",
            "num_items" : '.count($skus).'
          }
        }';

  $tmp = json_decode($json,true);
  //echo "<pre>".print_r(compact('json','tmp'),true)."</pre>"; die();
  $header_script .= "<script>
        //var window.dataLayer = window.dataLayer || [];
        dataLayer.push(".json_encode($tmp).");
      </script>
      ";

  echo $header_script;

?>